// const backendURL = 'https://awstestnowa.loca.lt';

// const backendURL = 'https://www.delilahresto.cf';


const frontURL = 'file:///C:/Users/Asus/Desktop/Delilah%20Vanilla!';

// const frontURL = 'http://d1lzmyody7azqe.cloudfront.net';

// const frontURL = 'https://www.delilahresto.cf/index/'


// const backendURL = 'http://localhost:3000';

const backendURL = 'http://localhost:4000';


const validator = async () => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

    let requestOptions = {
        method: 'POST',
        headers: myHeaders,
        redirect: 'follow'
    };


    await fetch(`${backendURL}/user/validateToken`, requestOptions)
        .then(response => resp = response.json())
        .catch(error => console.log('error', error));
    return resp;
}


const validateUserToken = () => {
 const returnPage = frontURL;
 
    try {
        if (window.sessionStorage.getItem('token') == undefined || window.sessionStorage.getItem('token') == null) {
            window.location.href = returnPage;
        } else {
            const response = validator()
                .then(response => {
                    if (response.status !== 200) {
                        window.location.href = returnPage;
                    }
                })
                .catch(error => {
                    console.log(error);
                    window.location.href = returnPage;
                });
        }
    } catch (error) {
        console.log(error);
        window.location.href = returnPage;
    }
}

