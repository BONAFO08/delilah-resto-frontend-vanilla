validateUserToken('../index.html');

const selectImg = (foodImgs) => {
  return Math.floor(Math.random() * foodImgs.length);
}





const createHomeProductDesk = (imgSelected) => {
  const img = imgSelected;
  const productDesk = html('div', {}, {});
  html('a', {
    margin: '30vh 0vh 0vh 10vh',
    backgroundImage: `url(${img})`
  }, { class: 'deskFrame', href: `${frontURL}${productsURL}` }, productDesk);
}


const saveImageProduct = (arrProducts) => {
  const foodImgs = arrProducts.map(products => products.imgUrl);
  sessionStorage.setItem('foodImgs', JSON.stringify(foodImgs))
  createHomeProductDesk(foodImgs[selectImg(foodImgs)])
}


const showProducts = () => {
  const foodImgs = JSON.parse(sessionStorage.getItem('foodImgs'));
  if (foodImgs == undefined || foodImgs == null) {
    getProducts()
      .then(resolve => saveImageProduct(resolve))
      .catch(error => console.log(error));
  } else {
    createHomeProductDesk(foodImgs[selectImg(foodImgs)])
  }
}

const stablishments = [
  {
    "city": "Provincia de Córdoba, Córdoba Capital",
    "address": "San Jerónimo 2051",
    "hours": "11:00 -21:00 hs Lun-Vie // 13:00 -23:00 hs Sab",
    "img": "https://a.cdn-hotels.com/gdcs/production0/d904/1a2db549-b49d-4845-aa19-c9d72f4266c2.jpg"
  },
  {
    "city": "Provincia de Santa Cruz, Río Gallegos",
    "address": "155, BCC, Zapiola",
    "hours": "11:00 -21:00 hs Lun-Vie // 13:00 -23:00 hs Sab",
    "img": "https://cdn.2001online.com/wp-content/uploads/2021/01/le-france.jpg"
  },
  {
    "city": "Provincia de Tierra del Fuego, Ushuaia",
    "address": "200 Gdor. Paz",
    "hours": "11:00 -21:00 hs Lun-Vie // 13:00 -23:00 hs Sab",
    "img": "https://www.renfe-sncf.com/es-es/blog/PublishingImages/paris-barrio-latino/paris_barrio_latino.jpg"
  }
];

const createHomeStablishmentsDesk = () => {
  const img = stablishments[selectImg(stablishments)].img;
  const stablishmentsDesk = html('div', {}, {});
  html('a', {
    margin: '30vh 0vh 0vh 110vh',
    backgroundImage: `url(${img})`
  }, { class: 'deskFrame' }, stablishmentsDesk);

}


showProducts()
createHomeStablishmentsDesk()




setCartImg('../imgs/shopCart.png')

