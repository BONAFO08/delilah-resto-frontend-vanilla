const getProducts = async () => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    await fetch(`${backendURL}/user/products`, requestOptions)
        .then(res => res.json())
        .then(response => resp = response)
        .catch(error => console.log('error', error));
    return resp;
}

