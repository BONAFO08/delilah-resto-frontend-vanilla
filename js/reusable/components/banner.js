
let noRepeatImageClick = false;

const homeURL = '/views/home.html';
const userManagerURL = '/views/managers/manager.users.html';
const productManagerURL = '/views/managers/manager.product.html';


const myAccountURL = '/views/user/myAccount.html';
const establishmentsURL = '/views/establishments/establishments.html';

const productsURL = '/views/shop/shop.products.html';
const newOrderURL = '/views/shop/shop.doOrder.html';

const isAdmin = (dropdownBannerContent) => {
    if (sessionStorage.getItem('sessionID') % 512 === 0) {
        html('a', {}, { href: `${frontURL}${myAccountURL}`, textContent: 'MI CUENTA' }, dropdownBannerContent)
        html('a', {}, { textContent: 'MIS PEDIDOS' }, dropdownBannerContent)
        html('a', {}, { href: `${frontURL}${userManagerURL}`, textContent: 'GESTOR DE USUARIOS' }, dropdownBannerContent)
        html('a', {}, { href: `${frontURL}${productManagerURL}`, textContent: 'GESTOR DE PRODUCTOS' }, dropdownBannerContent)
        html('a', {}, { textContent: 'GESTOR DE LOCALES' }, dropdownBannerContent)
        html('a', {}, { textContent: 'GESTOR DE PAGOS' }, dropdownBannerContent)
        html('a', {}, { textContent: 'GESTOR DE ORDENES' }, dropdownBannerContent)
        html('a', {}, { textContent: 'LOGOUT', id: 'logoutButtonBanner' }, dropdownBannerContent)

    } else {
        html('a', {}, { href: `${frontURL}${myAccountURL}`, textContent: 'MI CUENTA' }, dropdownBannerContent)
        html('a', {}, { textContent: 'MIS PEDIDOS' }, dropdownBannerContent)
        html('a', {}, { textContent: 'LOGOUT', id: 'logoutButtonBanner' }, dropdownBannerContent)
    }

    document.getElementById('logoutButtonBanner').addEventListener('click', () => {
        sessionStorage.clear()
        window.location.href = frontURL
    })
}


const createBanner = () => {
    const dropdownBanner = html('div', { cursor: 'pointer' }, { class: 'dropdownBanner' })
    dropdownBanner.addEventListener('click', (e) => {
        if (e.target.className == 'dropdownBanner') {
            window.location.href = `${frontURL}${homeURL}`;
        }
    })
    const dropButtonBanner = html('button', {}, { id: 'dropButtonBanner', class: 'dropButtonBanner' }, dropdownBanner)
    html('img', {}, { id: 'userImage', class: 'userImage' }, dropButtonBanner)
    const dropdownBannerContent = html('div', {}, { id: 'dropdownBannerContent', class: 'dropdownBannerContent' }, dropdownBanner)
    isAdmin(dropdownBannerContent)

    html('a',
        { margin: '19.8vh 0vh 0vh 0vh', },
        { textContent: 'INICIO', class: 'bannerOptions', href: `${frontURL}${homeURL}` })

    html('a',
        { margin: '19.8vh 0vh 0vh 17vh', },
        { textContent: 'NUESTROS PRODUCTOS', href: `${frontURL}${productsURL}`, class: 'bannerOptions', })

    html('a',
        { margin: '19.8vh 0vh 0vh 66.5vh', },
        { textContent: 'REALIZAR PEDIDO', href: `${frontURL}${newOrderURL}`, class: 'bannerOptions', })

    html('a',
        { margin: '19.8vh 0vh 0vh 105.5vh', },
        { textContent: 'NUESTROS LOCALES', href: `${frontURL}${establishmentsURL}`, class: 'bannerOptions', })






}


createBanner()

const openClose = (e) => {
    if (e.target.matches('.dropButtonBanner') || e.target.matches('.userImage')) {
        const dropdowns = document.getElementsByClassName("dropdownBannerContent");
        for (let i = 0; i < dropdowns.length; i++) {
            const openDropdown = dropdowns[i];
            if (e.target.matches('.userImage')) {
                noRepeatImageClickShopCart = !noRepeatImageClickShopCart
                if (noRepeatImageClickShopCart) {
                    document.getElementById("dropdownBannerContent").classList.toggle("showBanner");
                }
            } else {
                document.getElementById("dropdownBannerContent").classList.toggle("showBanner");
            }
        }
    }
}

const autoCloseDropDown = () => {
    window.onclick = function (event) {
        if (!event.target.matches('.dropButtonBanner') && !event.target.matches('.userImage')) {
            const dropdownsBanner = document.getElementsByClassName("dropdownBannerContent");

            for (let i = 0; i < dropdownsBanner.length; i++) {
                const openDropdown = dropdownsBanner[i];
                if (openDropdown.classList.contains('showBanner')) {
                    openDropdown.classList.remove('showBanner');
                }
            }
        }

        if (!event.target.matches('.dropButtonShopCart') && !event.target.matches('.cartImage') && !event.target.matches('.cartProductAmmountChanger')) {
            const dropdownsShopCart = document.getElementsByClassName("dropdownShopCartContent");
            for (let i = 0; i < dropdownsShopCart.length; i++) {
                const openDropdown = dropdownsShopCart[i];
                if (openDropdown.classList.contains('showShopCart')) {
                    openDropdown.classList.remove('showShopCart');
                }
            }
        }
    }
}

const fetchSendOrderToCollector = async (cart) => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

    let requestOptions = {
        method: 'POST',
        body: JSON.stringify(cart),
        headers: myHeaders,
        redirect: 'follow'
    };

    await fetch(`${backendURL}/orderCollector/saveOrder`, requestOptions)
        .then(response => resp = response.json())
        .catch(error => console.log('error', error));
    return resp;
}


const fetchLoadOrderInStanby = async () => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    await fetch(`${backendURL}/orderCollector/loadOrder`, requestOptions)
        .then(response => resp = response.json())
        .catch(error => console.log('error', error));
    return resp;
}


const loadOrderInStanby = async () => {
    return await fetchLoadOrderInStanby()
}

const sendOrderToCollector = async () => {
    const cart = sessionStorage.getItem('cart');
    const collectorFlag = JSON.parse(sessionStorage.getItem('cartSaved'))
    if (cart != null && cart != '[]') {
        if (collectorFlag == false || collectorFlag == null) {
            sessionStorage.setItem('cartSaved', true)
            fetchSendOrderToCollector({ arrFood: JSON.parse(cart) })
        }
    } else {
        const loaderFlag = JSON.parse(sessionStorage.getItem('cartLoaded'))
        const token = window.sessionStorage.getItem('token');
        if (loaderFlag == null && token != null) {
            sessionStorage.setItem('cartLoaded', true)
            const oldOrder = await loadOrderInStanby();
            if (oldOrder.status == 200) {
                const userResponse = window.confirm(`SE HA DETECTADO QUE YA TIENE UN PEDIDO NO CONFIRMADO  
                ¿LE GUSTARIA RETOMARLO?`)
                if (userResponse) {
                    sessionStorage.setItem('cart', JSON.stringify(oldOrder.data));
                    window.location.reload()
                }
            }
        }
    }
}


window.onclose = sendOrderToCollector()


autoCloseDropDown()

const testImage = (img) => {
    const tester = new Image();
    tester.onload = imageFound;
    tester.onerror = imageNotFound;
    tester.src = img;
}

const imageFound = () => {
    userImage.src = img;
}

const imageNotFound = () => {
    userImage.src = defaultImg;
    img = defaultImg;
}




const userImage = document.getElementById('userImage')
const defaultImg = 'https://upload.wikimedia.org/wikipedia/commons/d/d3/User_Circle.png';
let img = '';
try {
    img = JSON.parse(sessionStorage.getItem('userImg'));
} catch (error) {
    console.error(error);
    img = 'https://upload.wikimedia.org/wikipedia/commons/d/d3/User_Circle.png';
}

testImage(img)
userImage.style.width = '150px'
userImage.style.height = '148px'
userImage.style.backgroundPosition = 'center';
userImage.style.backgroundSize = 'cover';
userImage.style.borderRadius = '50%';
userImage.style.border = '1px solid black';

userImage.addEventListener('click', (e) => {
    openClose(e)
})


const dropButtonBanner = document.getElementById('dropButtonBanner')

dropButtonBanner.addEventListener('click', (e) => {
    openClose(e)
})