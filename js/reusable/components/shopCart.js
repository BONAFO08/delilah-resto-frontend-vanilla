
let noRepeatImageClickShopCart = false



const updateAmmountCounter = () => {
    let cartList = sessionStorage.getItem('cart');

    if (cartList == '[]' || cartList == null) {
        document.getElementById('textAmmount').textContent = 0;

    } else {
        cartList = JSON.parse(sessionStorage.getItem('cart'))
        document.getElementById('textAmmount').textContent = cartList.reduce((previousValue, currentValue) => previousValue + currentValue.ammount, 0)


    }

}


const hiddeProductShopButton = (shopButton) => {
    shopButton.disabled = true;
    shopButton.textContent = 'PRODUCTO EN EL CARRITO';
    shopButton.style.opacity = 0.3;
}

const showProductShopButton = (shopButton) => {
    shopButton.disabled = false;
    shopButton.textContent = 'AGREGAR AL CARRITO';
    shopButton.style.opacity = 1;
}

const addProductToWishList = (productData) => {
    let cartList = sessionStorage.getItem('cart');
    sessionStorage.setItem('cartSaved',false)

    if (cartList == null) {
        cartList = [{
            id: productData._id,
            name: productData.name,
            price: productData.price,
            ammount: 1
        }];
        sessionStorage.setItem('cart', JSON.stringify(cartList));
    } else {
        cartList = JSON.parse(cartList);
        cartList.push({
            id: productData._id,
            name: productData.name,
            price: productData.price,
            ammount: 1
        })
        sessionStorage.setItem('cart', JSON.stringify(cartList));
    }
    return cartList
}



const calculteTotalAmmountCart = () => {
    let cartList = sessionStorage.getItem('cart');
    if (cartList == '[]' || cartList == null) {
        return 0
    } else {
        cartList = JSON.parse(cartList);
        return cartList.reduce((previousValue, currentValue) => previousValue + currentValue.price * currentValue.ammount, 0)
    }
}


const incrementAmmountProduct = (id) => {
    let cartList = JSON.parse(sessionStorage.getItem('cart'));
    sessionStorage.setItem('cartSaved',false)

    const productSelected = cartList.filter(arr => arr.id == id)[0];
    const index = cartList.indexOf(productSelected);
    cartList[index].ammount += 1;
    sessionStorage.setItem('cart', JSON.stringify(cartList));
    document.getElementById(`item${id}ammount`).textContent = `CANTIDAD: ${cartList[index].ammount}`;
    document.getElementById(`billCart`).innerText = `TOTAL: \n \n $${calculteTotalAmmountCart(cartList)}`;
    updateAmmountCounter()
}

const decrementAmmountProduct = (id) => {
    let cartList = JSON.parse(sessionStorage.getItem('cart'));
    sessionStorage.setItem('cartSaved',false)

    const productSelected = cartList.filter(arr => arr.id == id)[0];
    const index = cartList.indexOf(productSelected);
    cartList[index].ammount -= 1;
    const newCartList = cartList.filter(arr => arr.id != id);
    if (productSelected.ammount <= 0) {
        document.getElementById(`item${id}Container`).remove()
        sessionStorage.setItem('cart', JSON.stringify(newCartList));
        document.getElementById(`billCart`).innerText = `TOTAL: \n \n $${calculteTotalAmmountCart(newCartList)}`;
        const shopButton = document.getElementById(`${id}Shop`);
        showProductShopButton(shopButton)
        updateAmmountCounter()
    } else {
        document.getElementById(`item${id}ammount`).textContent = `CANTIDAD: ${cartList[index].ammount}`;
        sessionStorage.setItem('cart', JSON.stringify(cartList));
        document.getElementById(`billCart`).innerText = `TOTAL: \n \n $${calculteTotalAmmountCart(cartList)}`;
        updateAmmountCounter()
    }
}

const removeProductFromCart = (id) => {
    let cartList = JSON.parse(sessionStorage.getItem('cart'));
    sessionStorage.setItem('cartSaved',false)

    const newCartList = cartList.filter(arr => arr.id != id);
    document.getElementById(`item${id}Container`).remove()
    sessionStorage.setItem('cart', JSON.stringify(newCartList));
    document.getElementById(`billCart`).innerText = `TOTAL: \n \n $${calculteTotalAmmountCart(newCartList)}`;
    const shopButton = document.getElementById(`${id}Shop`);
    updateAmmountCounter()
    showProductShopButton(shopButton)
}

const prepareCartList = (dropdownShopCartContent) => {
    clearDiv(dropdownShopCartContent)

    let cartList = sessionStorage.getItem('cart');

    if (cartList == '[]' || cartList == null) {
        html('h3', {}, { textContent: 'CARRITO VACIO', id: 'item0' }, dropdownShopCartContent)
    } else {
        cartList = JSON.parse(cartList);
        for (let i = 0; i < cartList.length; i++) {
            const cartProductContainer = html('div', { border: '3px solid #000000', cursor: 'default' }, { id: `item${cartList[i].id}Container` }, dropdownShopCartContent)

            cartProductContainer.addEventListener('mouseenter', () => {
                const items = document.getElementsByClassName(`item${cartList[i].id}`);
                for (let i = 0; i < items.length; i++) {
                    items[i].style.color = '#9bbeff';
                    items[i].style.textShadow = '1px 1px 1px #000000, 1px 1px 1px #000000, 1px 1px 1px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000';
                }
            })


            cartProductContainer.addEventListener('mouseleave', () => {
                const items = document.getElementsByClassName(`item${cartList[i].id}`);
                for (let i = 0; i < items.length; i++) {
                    items[i].style.color = '#588ae7';
                    items[i].style.textShadow = '';
                }
            })

            html('h3', {}, { textContent: cartList[i].name, class: `item${cartList[i].id}`, id: `item${cartList[i].id}name` }, cartProductContainer)
            html('h3', {}, { innerText: `PRECIO UNIDAD: \n \n $${cartList[i].price}`, class: `item${cartList[i].id}`, id: `item${cartList[i].id}price` }, cartProductContainer)
            html('h3', {}, { textContent: `CANTIDAD: ${cartList[i].ammount}`, class: `item${cartList[i].id}`, id: `item${cartList[i].id}ammount` }, cartProductContainer)

            const incrementAmmount = html('h3', { fontSize: '20px', padding: '2vh 0vh 2vh 0vh', cursor: 'pointer' },
                { textContent: `AUMENTAR CANTIDAD`, class: 'cartProductAmmountChanger' }, cartProductContainer)

            incrementAmmount.addEventListener('mouseenter', () => {
                incrementAmmount.style.color = '#000000';
                incrementAmmount.style.backgroundColor = '#58a345';
            })


            incrementAmmount.addEventListener('mouseleave', () => {
                incrementAmmount.style.color = '#588ae7';
                incrementAmmount.style.backgroundColor = '#284f96';
            })

            incrementAmmount.addEventListener('click', () => {
                incrementAmmountProduct(cartList[i].id)
            })

            const decreaseAmmountButton = html('h3', { fontSize: '20px', padding: '2vh 0vh 2vh 0vh', cursor: 'pointer' },
                { textContent: `DISMINUIR CANTIDAD`, class: 'cartProductAmmountChanger' }, cartProductContainer)

            decreaseAmmountButton.addEventListener('mouseenter', () => {
                decreaseAmmountButton.style.color = '#000000';
                decreaseAmmountButton.style.backgroundColor = '#ca9024';
            })


            decreaseAmmountButton.addEventListener('mouseleave', () => {
                decreaseAmmountButton.style.color = '#588ae7';
                decreaseAmmountButton.style.backgroundColor = '#284f96';
            })

            decreaseAmmountButton.addEventListener('click', () => {
                decrementAmmountProduct(cartList[i].id)
            })


            const removeProductFromCartButton = html('h3', { fontSize: '20px', padding: '2vh 0vh 2vh 0vh', cursor: 'pointer' },
                { textContent: `QUITAR DEL CARRITO`, class: 'cartProductAmmountChanger' }, cartProductContainer)

            removeProductFromCartButton.addEventListener('mouseenter', () => {
                removeProductFromCartButton.style.color = '#000000';
                removeProductFromCartButton.style.backgroundColor = '#b85353';
            })


            removeProductFromCartButton.addEventListener('mouseleave', () => {
                removeProductFromCartButton.style.color = '#588ae7';
                removeProductFromCartButton.style.backgroundColor = '#284f96';
            })

            removeProductFromCartButton.addEventListener('click', () => {
                removeProductFromCart(cartList[i].id)
            })

            if (i == cartList.length - 1) {
                const cartTotalAmmountContainer = html('div', { border: '3px solid #000000', cursor: 'default' }, {}, dropdownShopCartContent)
                html('h3', {}, { innerText: `TOTAL: \n \n $${calculteTotalAmmountCart(cartList)}`, id: `billCart` }, cartTotalAmmountContainer)
                const confirmOrderButton = html('h3', { border: '1px solid #000000', backgroundColor: '#366941', color: '#000000', cursor: 'pointer' }, { innerText: `CONFIRMAR PEDIDO` }, cartTotalAmmountContainer)

                confirmOrderButton.addEventListener('mouseenter', () => {
                    confirmOrderButton.style.color = '#276935';
                    confirmOrderButton.style.backgroundColor = '#5ed678';
                })


                confirmOrderButton.addEventListener('mouseleave', () => {
                    confirmOrderButton.style.color = '#000000';
                    confirmOrderButton.style.backgroundColor = '#366941';
                })

                confirmOrderButton.addEventListener('click',()=>{
                    window.location.href = `${frontURL}${newOrderURL}`; 
                })
            }

        }
    }
}


const addToCart = (productData) => {
    const dropdownShopCartContent = document.getElementById('dropdownShopCartContent');
    let cartList = sessionStorage.getItem('cart');


    if (productData == undefined && (cartList == null || cartList == '[]')) {
        html('h3', {}, { textContent: 'CARRITO VACIO', id: 'item0' }, dropdownShopCartContent)
    } else if (productData != undefined) {
        cartList = addProductToWishList(productData)
        prepareCartList(dropdownShopCartContent)
    } else {
        cartList = JSON.parse(cartList);
        prepareCartList(dropdownShopCartContent)
    }

}

const createShopCart = () => {
    const dropdownShopCart = html('div', { cursor: 'pointer' }, { class: 'dropdownShopCart' })
    dropdownShopCart.addEventListener('click', (e) => {
        if (e.target.className == 'dropdownShopCart') {
            window.location.href = `${frontURL}${homeURL}`;
        }
    })
    const dropButtonShopCart = html('button', {}, { id: 'dropButtonShopCart', class: 'dropButtonShopCart' }, dropdownShopCart)
    html('h3', {
        display: 'inline',
        position: 'absolute',
        margin: '14vh 0vh 0vh -1.5vh',
        border: '2px solid #000000',
        backgroundColor: '#ce912f',
        color: '#000000',
        padding: '5px',
        borderRadius: '50%',
        fontFamily: "'Josefin Sans', sans-serif",

    }, { class: 'textAmmount', id: 'textAmmount' }, dropButtonShopCart)
    html('img', {}, { id: 'cartImage', class: 'cartImage' }, dropButtonShopCart)
    const dropdownShopCartContent = html('div', {}, { id: 'dropdownShopCartContent', class: 'dropdownShopCartContent' }, dropdownShopCart)
    addToCart();
}


createShopCart()

const openCloseShopCart = (e) => {
    if (e.target.matches('.dropButtonShopCart') || e.target.matches('.cartImage')) {
        const dropdowns = document.getElementsByClassName("dropdownShopCartContent");
        for (let i = 0; i < dropdowns.length; i++) {
            const openDropdown = dropdowns[i];
            if (e.target.matches('.cartImage')) {
                noRepeatImageClickShopCart = !noRepeatImageClickShopCart
                if (noRepeatImageClickShopCart) {
                    document.getElementById("dropdownShopCartContent").classList.toggle("showShopCart");
                }
            } else {
                document.getElementById("dropdownShopCartContent").classList.toggle("showShopCart");
            }
        }
    }
}



const setCartImg = (urlImgCart) => {
    const cartImage = document.getElementById('cartImage')
    const img = `${urlImgCart}`;
    cartImage.src = img;
    cartImage.style.width = '150px'
    cartImage.style.height = '148px'
    cartImage.style.backgroundPosition = 'center';
    cartImage.style.backgroundSize = 'cover';
    cartImage.style.borderRadius = '50%';
    cartImage.style.border = '1px solid black';

    cartImage.addEventListener('click', (e) => {
        openCloseShopCart(e)
    })
}

const dropButtonShopCart = document.getElementById('dropButtonShopCart')

dropButtonShopCart.addEventListener('click', (e) => {
    openCloseShopCart(e)
})

updateAmmountCounter()