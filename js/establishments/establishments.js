validateUserToken('../../index.html');
setCartImg('../../imgs/shopCart.png');


const getEstablishments = async () => {
    return  [
            {
                "city": "Provincia de Córdoba, Córdoba Capital",
                "address": "San Jerónimo 2051",
                "hours": "11:00 -21:00 hs Lun-Vie // 13:00 -23:00 hs Sab",
                "imgUrl": "https://a.cdn-hotels.com/gdcs/production0/d904/1a2db549-b49d-4845-aa19-c9d72f4266c2.jpg",
                "_id": "123124qse"
            },
            {
                "city": "Provincia de Santa Cruz, Río Gallegos",
                "address": "155, BCC, Zapiola",
                "hours": "11:00 -21:00 hs Lun-Vie // 13:00 -23:00 hs Sab",
                "imgUrl": "https://cdn.2001online.com/wp-content/uploads/2021/01/le-france.jpg",
                "_id": "1231asdasda12324qse"
            },
            {
                "city": "Provincia de Tierra del Fuego, Ushuaia",
                "address": "200 Gdor. Paz",
                "hours": "11:00 -21:00 hs Lun-Vie // 13:00 -23:00 hs Sab",
                "imgUrl": "https://www.renfe-sncf.com/es-es/blog/PublishingImages/paris-barrio-latino/paris_barrio_latino.jpg",
                "_id": "123fdff123qse"
            }
        ]
    }



const showEstablishments = async () => {
    const establishmentsData = await getEstablishments()

    for (let i = 0; i < establishmentsData.length; i++) {
        createEstablishmentsCard(establishmentsData[i], i, establishmentsData.length)
    }
}


const createEstablishmentsCard = (establishment, index, endOfTheList) => {
 
    const establishmentContainer = html('div',
        {
            position: 'absolute',
            width: "fit-content",
            height: "fit-content",
            margin: `${(30 * (index + 1)) + (30 * index)}vh 0vh 0vh 60vh`,
            backgroundColor: '#000000',
            border: "5px solid #000000 ",
            borderRadius: "10%",
            fontFamily: "'Josefin Sans', sans-serif",
            transition: "all 1.5s",
            textShadow: '2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000',
            color: '#bdc4ff',

        },


        {
            id: `${establishment._id}Container`,
            class: 'establishmentContainer'
        });



    establishmentContainer.addEventListener('mouseenter', () => {
        onEnterEstablishmentContainer(establishment._id, index)

    })


    establishmentContainer.addEventListener('mouseleave', () => {
        onLeaveEstablishmentContainer(establishment._id, index)

    })



    establishment
    const establishmentImg = html('img',
        {
            display: 'inline',
            width: '70vh',
            height: '45vh',
            margin: "0vh 0vh 0vh 0vh",
            borderRadius: '10%',
            border: "5px solid #000000 ",
        },
        {
            id: `${establishment._id}Img`,
            src: establishment.imgUrl
        },
        establishmentContainer);

    const establishmentCity = html('h3',
        {
            display: 'none',
            position: "absolute",
            fontSize: "40px",
            margin: "0vh 0vh 0vh 10vh"
        },
        {
            id: `${establishment._id}City`,
            textContent: establishment.city.toUpperCase()

        },
        establishmentContainer);

    const establishmentAddress = html('h3',
        {
            display: 'none',
            position: "absolute",
            fontSize: "40px",
            margin: "18vh 0vh 0vh 10vh"
        },
        {
            id: `${establishment._id}Address`,
            textContent: `DIRECCIÓN: ${establishment.address.toUpperCase()}`

        },
        establishmentContainer);

    const establishmentHours = html('h3',
        {
            display: 'none',
            position: "absolute",
            fontSize: "40px",
            margin: "35vh 0vh 0vh 10vh"
        },
        {
            id: `${establishment._id}Hours`,
            textContent: `${establishment.hours}`

        },
        establishmentContainer);

    if (endOfTheList == index + 1) {
        html('div',
            {
                display: 'inline',
                position: "absolute",
                padding: '20vh 0vh 0vh 0vh',
                border: '2px solid transparent',
                margin: `${(30 * (index + 3)) + (30 * index + 1)}vh 0vh 0vh 60vh`,
            });
    }
}


const onEnterEstablishmentContainer = (_id, index) => {
    const containerStyle = document.getElementById(`${_id}Container`).style;

    containerStyle.width = "180vh";
    containerStyle.height = "50vh";
    containerStyle.padding = "5vh 0vh 0vh 0vh";

    containerStyle.margin = `${(30 * (index + 1)) + (30 * index)}vh 0vh 0vh 0vh`;
    containerStyle.backgroundColor = "#7179ea";

    const cityStyle = document.getElementById(`${_id}City`).style;
    cityStyle.display = 'inline';

    const addressStyle = document.getElementById(`${_id}Address`).style;
    addressStyle.display = 'inline';

    const hoursStyle = document.getElementById(`${_id}Hours`).style;
    hoursStyle.display = 'inline';

    const imgStyle = document.getElementById(`${_id}Img`).style;
    imgStyle.margin = "0vh 0vh 0vh 0.5vh";

}

const onLeaveEstablishmentContainer = (_id, index) => {

    const containerStyle = document.getElementById(`${_id}Container`).style;

    containerStyle.width = "fit-content";
    containerStyle.height = "fit-content";
    containerStyle.margin = `${(30 * (index + 1)) + (30 * index)}vh 0vh 0vh 60vh`;
    containerStyle.backgroundColor = "#000000";
    containerStyle.padding = "0vh 0vh 0vh 0vh";


    const cityStyle = document.getElementById(`${_id}City`).style;
    cityStyle.display = 'none';

    const addressStyle = document.getElementById(`${_id}Address`).style;
    addressStyle.display = 'none';

    const hoursStyle = document.getElementById(`${_id}Hours`).style;
    hoursStyle.display = 'none';

    const shopStyle = document.getElementById(`${_id}Shop`).style;
    shopStyle.display = 'none';

    const imgStyle = document.getElementById(`${_id}Img`).style;
    imgStyle.margin = "0vh 0vh 0vh 0vh";
}


showEstablishments()

