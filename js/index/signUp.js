
const createUser = async (dataInput) => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    let requestOptions = {
        method: 'POST',
        body: JSON.stringify(dataInput),
        headers: myHeaders,
        redirect: 'follow'
    };

    await fetch(`${backendURL}/user/signUp`, requestOptions)
        .then(response => response.json())
        .then(result => resp = result)
        .catch(error => console.log('error', error));
    return resp;

}


const signUp = () => {
    const userData = {};
    userData.username = document.getElementById('usernameSignUpInput').value;
    userData.name = document.getElementById('nameSignUpInput').value;
    userData.address = document.getElementById('addressSignUpInput').value;
    userData.email = document.getElementById('emailSignUpInput').value;
    userData.phone = document.getElementById('phoneSignUpInput').value;
    userData.password = document.getElementById('passwordSignUpInput').value;


    const validator =
        userData.username.trim().length !== 0 &&
        userData.name.trim().length !== 0 &&
        userData.address.trim().length !== 0 &&
        userData.phone.length !== 0 &&
        userData.email.trim().length !== 0 &&
        userData.password.trim().length !== 0;

    document.getElementById('usernameSignUpInput').value = '';
    document.getElementById('nameSignUpInput').value = '';
    document.getElementById('addressSignUpInput').value = '';
    document.getElementById('emailSignUpInput').value = '';
    document.getElementById('phoneSignUpInput').value = '';
    document.getElementById('passwordSignUpInput').value = '';


    if (validator === true) {
        const response = createUser(userData)
            .then(response => {
                if (response.status === 200) {
                    clearDiv(cardIndexDiv)
                    showLogInScreen()
                } else {
                    document.getElementById('responseSignUp').style.color = '#be1313';
                    document.getElementById('responseSignUp').textContent = response.msj;
                }
            })
            .catch(error => console.log(error));
    } else {
        document.getElementById('responseSignUp').style.color = '#be1313';
        document.getElementById('responseSignUp').textContent = 'INFORMACIÓN INVALIDA'; 

    }

}


const showSignUpScreen = () => {
    cardIndexDiv.style.height = '94vh';
    cardIndexDiv.style.width = '160vh';
    cardIndexDiv.style.margin = '2vh 20vh';

    setTimeout(() => {
        const titleSignUp = html('h3',
            {
                fontFamily: 'Lato',
                fontSize: '7vh',
                textAlign: 'center',
                textShadow: '#474747 3px 2px 2px',
            },
            { id: 'titleSignUp', textContent: 'BIENVENIDO/A A LA DELILAH FAMILY' },
            cardIndexDiv);

        const usernameSignUpTag = html('h3',
            {
                display: 'inline',
                position: 'absolute',
                fontSize: '3.5vh',
                fontFamily: 'Lato',
                color: '#588ae7',
                textShadow: '2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000',
                margin: "5vh 0vh 0vh 14vh"
            },
            { id: 'usernameSignUpTag', textContent: 'NOMBRE DE USUARIO' },
            cardIndexDiv);

        const usernameSignUpInput = html('input',
            {
                padding: '0.5vh',
                fontSize: '4vh',
                borderRadius: '5%',
                border: '3.5px solid #000000',
                margin: "10vh 0vh 0vh 10vh"
            },
            {
                id: 'usernameSignUpInput',
                required: 'true',
                type: 'text',
                name: 'username',
                placeholder: 'usuario582514'
            },
            cardIndexDiv);


        const nameSignUpTag = html('h3',
            {
                display: 'inline',
                position: 'absolute',
                fontFamily: 'Lato',
                fontSize: '3.5vh',
                color: '#588ae7',
                textShadow: '2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000',
                margin: "5vh 0vh 0vh 28vh"
            },
            { id: 'nameSignUpTag', textContent: 'NOMBRE Y APELLIDO' },
            cardIndexDiv);

        const nameSignUpInput = html('input',
            {
                padding: '0.5vh',
                fontSize: '4vh',
                borderRadius: '5%',
                border: '3.5px solid #000000',
                margin: "10vh 0vh 0vh 24vh"
            },
            {
                id: 'nameSignUpInput',
                required: 'true',
                type: 'text',
                name: 'name',
                placeholder: 'Delilah Gutierres'
            },
            cardIndexDiv);


        html('br',
            {
            },
            {
            },
            cardIndexDiv)

        const addressSignUpTag = html('h3',
            {
                display: 'inline',
                position: 'absolute',
                fontSize: '3.5vh',
                fontFamily: 'Lato',
                color: '#588ae7',
                textShadow: '2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000',
                margin: "3vh 0vh 0vh 24vh"
            },
            { id: 'addressSignUpTag', textContent: 'DOMICILIO' },
            cardIndexDiv);

        const addressSignUpInput = html('input',
            {
                padding: '0.5vh',
                fontSize: '4vh',
                borderRadius: '5%',
                border: '3.5px solid #000000',
                margin: "10vh 0vh 0vh 10vh"
            },
            {
                id: 'addressSignUpInput',
                required: 'true',
                type: 'text',
                name: 'address',
                placeholder: 'Calle Ejemplo 123'
            },
            cardIndexDiv);


        const emailSignUpTag = html('h3',
            {
                display: 'inline',
                position: 'absolute',
                fontSize: '3.5vh',
                fontFamily: 'Lato',
                color: '#588ae7',
                textShadow: '2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000',
                margin: "3vh 0vh 0vh 28vh"
            },
            { id: 'emailSignUpTag', textContent: 'CORREO ELECTRÓNICO' },
            cardIndexDiv);

        const emailSignUpInput = html('input',
            {
                padding: '0.5vh',
                fontSize: '4vh',
                borderRadius: '5%',
                border: '3.5px solid #000000',
                margin: "10vh 0vh 0vh 24vh"
            },
            {
                id: 'emailSignUpInput',
                required: 'true',
                type: 'email',
                name: 'email',
                placeholder: 'ejemplo@gmail.com'
            },
            cardIndexDiv);


        html('br',
            {
            },
            {
            },
            cardIndexDiv)

        const phoneSignUpTag = html('h3',
            {
                display: 'inline',
                position: 'absolute',
                fontSize: '3.5vh',
                color: '#588ae7',
                fontFamily: 'Lato',
                textShadow: '2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000',
                margin: "3vh 0vh 0vh 14vh"
            },
            { id: 'phoneSignUpTag', textContent: 'NÚMERO DE TELÉFONO' },
            cardIndexDiv);

        const phoneSignUpInput = html('input',
            {
                padding: '0.5vh',
                fontSize: '4vh',
                borderRadius: '5%',
                border: '3.5px solid #000000',
                margin: "10vh 0vh 0vh 10vh"
            },
            {
                id: 'phoneSignUpInput',
                required: 'true',
                type: 'number',
                name: 'phone',
                placeholder: '0123456789'
            },
            cardIndexDiv);


        const passwordSignUpTag = html('h3',
            {
                display: 'inline',
                position: 'absolute',
                fontSize: '3.5vh',
                fontFamily: 'Lato',
                color: '#588ae7',
                textShadow: '2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000',
                margin: "3vh 0vh 0vh 35vh"
            },
            { id: 'passwordSignUpTag', textContent: 'CONTRASEÑA' },
            cardIndexDiv);

        const passwordSignUpInput = html('input',
            {
                padding: '0.5vh',
                fontSize: '4vh',
                borderRadius: '5%',
                border: '3.5px solid #000000',
                margin: "10vh 0vh 0vh 24vh"
            },
            {
                id: 'passwordSignUpInput',
                required: 'true',
                type: 'password',
                name: 'password',
                placeholder: ''
            },
            cardIndexDiv);

        html('br',
            {
            },
            {
            },
            cardIndexDiv)

        const responseSignUp = html('h3',
            {
                display: 'inline',
                position: 'absolute',
                fontSize: '3.5vh',
                fontFamily: 'Lato',
                color: '#588ae7',
                textShadow: '2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000',
                margin: "7vh 0vh 0vh 35vh"
            },
            { id: 'responseSignUp', textContent: '' },
            cardIndexDiv);

        const backButtonSignUp = html('button',
            {
                display: 'inline',
                position: 'absolute',
                fontSize: '5vh',
                border: '0.5vh solid #000000',
                background: '#284f96',
                cursor: 'pointer',
                borderRadius: '30%',
                padding: '2%',
                transition: 'all 0.4s',
                margin: '18vh 0vh 0vh 23.5vh',
                zIndex: 1,
            },
            { id: 'backButtonSignUp', textContent: 'VOLVER' },
            cardIndexDiv);

        backButtonSignUp.addEventListener('mouseenter', () => {
            backButtonSignUp.style.background = '#588ae7';
            backButtonSignUp.style.padding = '2% 5% 2% 5%';
            backButtonSignUp.style.margin = '18vh 0vh 0vh 18.5vh'

        })

        backButtonSignUp.addEventListener('mouseleave', () => {
            backButtonSignUp.style.background = '#284f96';
            backButtonSignUp.style.padding = '2%';
            backButtonSignUp.style.margin = '18vh 0vh 0vh 23.5vh'

        })

        backButtonSignUp.addEventListener('click', () => {
            clearDiv(cardIndexDiv)
            showIndexScreen()
        })

        const sendButtonSignUp = html('button',
            {
                display: 'inline',
                position: 'absolute',
                fontSize: '5vh',
                border: '0.5vh solid #000000',
                background: '#284f96',
                cursor: 'pointer',
                borderRadius: '30%',
                padding: '2%',
                transition: 'all 0.4s',
                margin: '18vh 0vh 0vh 63.5vh',
                zIndex: 1,
            },
            { id: 'sendButtonSignUp', textContent: 'ENVIAR' },
            cardIndexDiv);

        sendButtonSignUp.addEventListener('mouseenter', () => {
            sendButtonSignUp.style.background = '#588ae7';
            sendButtonSignUp.style.padding = '2% 5% 2% 5%';
            sendButtonSignUp.style.margin = '18vh 0vh 0vh 58.5vh'

        })

        sendButtonSignUp.addEventListener('mouseleave', () => {
            sendButtonSignUp.style.background = '#284f96';
            sendButtonSignUp.style.padding = '2%';
            sendButtonSignUp.style.margin = '18vh 0vh 0vh 63.5vh'

        })

        sendButtonSignUp.addEventListener('click', () => {
            signUp()
        })

        const loginButtonSignUp = html('button',
            {
                display: 'inline',
                position: 'absolute',
                fontSize: '4.5vh',
                border: '0.5vh solid #000000',
                background: '#284f96',
                cursor: 'pointer',
                borderRadius: '30%',
                padding: '2%',
                transition: 'all 0.4s',
                margin: '15vh 0vh 0vh 103.5vh',
                zIndex: 1,
            },
            { id: 'loginButtonSignUp', innerText: `YA TENGO \n UNA CUENTA` },
            cardIndexDiv);

        loginButtonSignUp.addEventListener('mouseenter', () => {
            loginButtonSignUp.style.background = '#588ae7';
            loginButtonSignUp.style.padding = '2% 5% 2% 5%';
            loginButtonSignUp.style.margin = '15vh 0vh 0vh 98.5vh'

        })

        loginButtonSignUp.addEventListener('mouseleave', () => {
            loginButtonSignUp.style.background = '#284f96';
            loginButtonSignUp.style.padding = '2%';
            loginButtonSignUp.style.margin = '15vh 0vh 0vh 103.5vh'

        })

        loginButtonSignUp.addEventListener('click', () => {
            clearDiv(cardIndexDiv)
            showLogInScreen(400)
        })
    }, 400);


}




