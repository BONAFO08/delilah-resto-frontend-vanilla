


const selectImgBackground = () => {
    return Math.floor(Math.random() * backgroundIndexImgs.length);
}


const backgroundIndexImgs = [
    "https://cdn.vox-cdn.com/thumbor/9qN-DmdwZE__GqwuoJIinjUXzmk=/0x0:960x646/1200x900/filters:focal(404x247:556x399)/cdn.vox-cdn.com/uploads/chorus_image/image/63084260/foodlife_2.0.jpg",
    "https://media.istockphoto.com/photos/arabic-and-middle-eastern-dinner-table-hummus-tabbouleh-salad-salad-picture-id1175505781?k=20&m=1175505781&s=612x612&w=0&h=STomby2lCtcvpl_hxK6RhknQQWrkvpkHcoDLD4zttFk=",
    "https://pizzapalaceburwell.com/wp-content/uploads/2021/11/Food.jpg"
];


//BACKGROUND INDEX
const indexDiv = html('div', {
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    height: '100vh',
    backgroundImage: `url("${backgroundIndexImgs[selectImgBackground()]}")`
}, { id: 'indexDiv' });


//CARD DIV
const cardIndexDiv = html('div',
    {
        position: 'absolute',
        border: '0.5vh solid #150070',
        background: '#3462b5',
        background: 'linear-gradient(180deg, #3462b5 0%, #3c3cba 13%, #00d4ff 100%)',
        borderRadius: '10%',
        fontFamily: "'Montserrat', sans-serif",
        transition: 'all 0.5s'
    },
    { id: 'cardIndexDiv' },
    indexDiv);

const showIndexScreen = () => {
    
    


    cardIndexDiv.style.height = '96vh';
    cardIndexDiv.style.width = '70vh';
    cardIndexDiv.style.margin = '2vh 70vh';

    //BACKGROUND TITLE
    const titleIndex = html('h3',
        {
            fontFamily: 'Lato',
            fontSize: '7vh',
            textAlign: 'center',
            textShadow: '#474747 3px 2px 2px',
        },
        { id: 'titleIndex', textContent: 'DELILAH RESTO' },
        cardIndexDiv);


    const signupIndexButton = html('button',
        {
            display: 'inline',
            position: 'absolute',
            fontSize: '5vh',
            border: '0.5vh solid #000000',
            background: '#284f96',
            cursor: 'pointer',
            borderRadius: '30%',
            padding: '3%',
            transition: 'all 0.4s',
            margin: '16vh 0vh 0vh 13vh',
            zIndex: 1,
        },
        { id: 'signupIndexButton', textContent: 'CREAR CUENTA' },
        cardIndexDiv);

    signupIndexButton.addEventListener('mouseenter', () => {
        signupIndexButton.style.background = '#588ae7';
        signupIndexButton.style.padding = '3% 10% 3% 10%';
        signupIndexButton.style.margin = '16vh 0vh 0vh 8vh'

    })

    signupIndexButton.addEventListener('mouseleave', () => {
        signupIndexButton.style.background = '#284f96';
        signupIndexButton.style.padding = '3%';
        signupIndexButton.style.margin = '16vh 0vh 0vh 13vh'

    })

    signupIndexButton.addEventListener('click', () => {
        clearDiv(cardIndexDiv)
        showSignUpScreen()
    })

    const loginIndexButton = html('button',
        {
            display: 'inline',
            position: 'absolute',
            fontSize: '5vh',
            border: '0.5vh solid #000000',
            background: '#284f96',
            cursor: 'pointer',
            borderRadius: '30%',
            padding: '3%',
            transition: 'all 0.4s',
            margin: '36vh 0vh 0vh 13.5vh',
            zIndex: 1,
        },
        { id: 'loginIndexButton', textContent: 'INICIAR SESIÓN' },
        cardIndexDiv);

    loginIndexButton.addEventListener('mouseenter', () => {
        loginIndexButton.style.background = '#588ae7';
        loginIndexButton.style.padding = '3% 10% 3% 10%';
        loginIndexButton.style.margin = '36vh 0vh 0vh 8.5vh'

    })

    loginIndexButton.addEventListener('mouseleave', () => {
        loginIndexButton.style.background = '#284f96';
        loginIndexButton.style.padding = '3%';
        loginIndexButton.style.margin = '36vh 0vh 0vh 13.5vh'


    })

    loginIndexButton.addEventListener('click', () => {
        clearDiv(cardIndexDiv)
        showLogInScreen(200)
    })


    const googleIndexButton = html('button',
        {
            position: 'absolute',
            fontSize: '5vh',
            border: ' 0.5vh solid #000000',
            backgroundColor: '#b9b9b9',
            backgroundImage: "url('https://rotulosmatesanz.com/wp-content/uploads/2017/09/2000px-Google_G_Logo.svg_.png')",
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            cursor: 'pointer',
            padding: '10%',
            transition: 'opacity 0.4s',
            borderRadius: '80%',
            opacity: 0.4,
            margin: "58vh 0vh 0vh 10vh",
            zIndex: 1,
        },
        { id: 'googleIndexButton' },
        cardIndexDiv);

    googleIndexButton.addEventListener('mouseenter', () => {
        googleIndexButton.style.opacity = 1;

    })

    googleIndexButton.addEventListener('mouseleave', () => {
        googleIndexButton.style.opacity = 0.4;

    })

    googleIndexButton.addEventListener('click', () => {
        window.location.href = `${backendURL}/auth/google/callback`
    })

}


showIndexScreen()


