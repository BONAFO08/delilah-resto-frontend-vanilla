
const fetchLogIn = async (dataInput) => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    let requestOptions = {
        method: 'POST',
        body: JSON.stringify(dataInput),
        headers: myHeaders,
        redirect: 'follow'
    };

    await fetch(`${backendURL}/user/logIn`, requestOptions)
        .then(response => resp = response.json())
        .catch(error => console.log('error', error));
    return resp;
}

const logIn = () => {
    const userData = {};
    userData.name = document.getElementById('usernameLogInInput').value;
    userData.password = document.getElementById('passwordLogInInput').value;


    const validator =
        userData.name.trim().length !== 0 &&
        userData.password.trim().length !== 0;

    document.getElementById('usernameLogInInput').value = '';
    document.getElementById('passwordLogInInput').value = '';

    if (validator === true) {
        const response = fetchLogIn(userData)
            .then(response => {
                if (response.status === 200) {
                    window.sessionStorage.setItem('token', response.token);
                    window.sessionStorage.setItem('sessionID', response.sessionID);
                    (response.userImg == undefined)
                        ? (window.sessionStorage.setItem('userImg', 'https://upload.wikimedia.org/wikipedia/commons/d/d3/User_Circle.png'))
                        : (window.sessionStorage.setItem('userImg', JSON.stringify(response.userImg)))

                    window.location.href = './views/home.html';
                } else {
                    document.getElementById('responseLogInTag').style.color = '#be1313';
                    document.getElementById('responseLogInTag').textContent = response.msj;
                }
            })
            .catch(error => console.log(error));
    } else {
        document.getElementById('responseLogInTag').style.color = '#be1313';
        document.getElementById('responseLogInTag').textContent = 'INFORMACIÓN INVALIDA';

    }

}

const showLogInScreen = (timeout) => {
    cardIndexDiv.style.height = '96vh';
    cardIndexDiv.style.width = '70vh';
    cardIndexDiv.style.margin = '2vh 70vh';

    const titleLogIn = html('h3',
        {
            fontFamily: 'Lato',
            fontSize: '7vh',
            textAlign: 'center',
            textShadow: '#474747 3px 2px 2px',
        },
        { id: 'titleLogIn', textContent: 'DELILAH RESTO' },
        cardIndexDiv);

    setTimeout(() => {
        const usernameLogInTag = html('h3',
            {
                fontFamily: 'Lato',
                fontSize: '4vh',
                textAlign: 'center',
                color: '#588ae7',
                textShadow: '2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000',
                margin: '4vh 0vh 0vh 0vh',
            },
            { id: 'usernameLogInTag', textContent: 'NOMBRE DE USUARIO O CORREO ELECTRÓNICO' },
            cardIndexDiv);

        const usernameLogInInput = html('input',
            {
                padding: '1vh',
                fontSize: '4vh',
                borderRadius: '5%',
                border: '3.5px solid #000000',
                margin: "2vh 0vh 0vh 10vh"
            },
            {
                id: 'usernameLogInInput',
                required: 'true',
                type: 'text',
                name: 'username',
                placeholder: 'usuario582514'
            },
            cardIndexDiv);


        const passowrdLogInTag = html('h3',
            {
                fontFamily: 'Lato',
                fontSize: '4vh',
                textAlign: 'center',
                color: '#588ae7',
                textShadow: '2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000',
                margin: '2.5vh 0vh 0vh 0vh',
            },
            { id: 'passowrdLogInTag', textContent: 'CONTRASEÑA' },
            cardIndexDiv);

        const passwordLogInInput = html('input',
            {
                padding: '1vh',
                fontSize: '4vh',
                borderRadius: '5%',
                border: '3.5px solid #000000',
                margin: "3vh 0vh 0vh 10vh"
            },
            {
                id: 'passwordLogInInput',
                required: 'true',
                type: 'password',
                name: 'password',
                placeholder: ''
            },
            cardIndexDiv);

        const responseLogInTag = html('h3',
            {
                fontFamily: 'Lato',
                fontSize: '4vh',
                textAlign: 'center',
                color: '#588ae7',
                textShadow: '2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000',
                margin: '2.5vh 0vh 0vh 0vh',
            },
            { id: 'responseLogInTag', textContent: '' },
            cardIndexDiv);

        const sendButtonLogin = html('button',
            {
                fontSize: '5vh',
                border: '0.5vh solid #000000',
                background: '#284f96',
                cursor: 'pointer',
                borderRadius: '30%',
                padding: '2%',
                transition: 'all 0.4s',
                margin: '6vh 0vh 0vh 23.5vh',
                zIndex: 1,
            },
            { id: 'sendButtonLogin', textContent: 'ENVIAR' },
            cardIndexDiv);

        sendButtonLogin.addEventListener('mouseenter', () => {
            sendButtonLogin.style.background = '#588ae7';
            sendButtonLogin.style.padding = '2% 10% 2% 10%'
            sendButtonLogin.style.margin = '6vh 0vh 0vh 18.5vh'

        })


        sendButtonLogin.addEventListener('mouseleave', () => {
            sendButtonLogin.style.background = '#284f96';
            sendButtonLogin.style.padding = '2%';
            sendButtonLogin.style.margin = '6vh 0vh 0vh 23.5vh'

        })

        sendButtonLogin.addEventListener('click', () => {
            logIn()
        })

        const signUpButtonLogin = html('button',
            {
                fontSize: '5vh',
                border: '0.5vh solid #000000',
                background: '#284f96',
                cursor: 'pointer',
                borderRadius: '30%',
                padding: '2%',
                transition: 'all 0.4s',
                margin: '2vh 0vh 0vh 14.5vh',
                zIndex: 1,
            },
            { id: 'sendButtonLogin', textContent: 'REGISTRARME' },
            cardIndexDiv);

        signUpButtonLogin.addEventListener('mouseenter', () => {
            signUpButtonLogin.style.background = '#588ae7';
            signUpButtonLogin.style.padding = '2% 10% 2% 10%'
            signUpButtonLogin.style.margin = '2vh 0vh 0vh 9.5vh'


        })

        signUpButtonLogin.addEventListener('mouseleave', () => {
            signUpButtonLogin.style.background = '#284f96';
            signUpButtonLogin.style.padding = '2%';
            signUpButtonLogin.style.margin = '2vh 0vh 0vh 14.5vh'
        })

        signUpButtonLogin.addEventListener('click', () => {
            clearDiv(cardIndexDiv)
            showSignUpScreen()
        })

        const backButtonLogin = html('button',
            {
                fontSize: '5vh',
                border: '0.5vh solid #000000',
                background: '#284f96',
                cursor: 'pointer',
                borderRadius: '30%',
                padding: '2%',
                transition: 'all 0.4s',
                margin: '2vh 0vh 0vh 23.5vh',
                zIndex: 1,
            },
            { id: 'backButtonLogin', textContent: 'VOLVER' },
            cardIndexDiv);

        backButtonLogin.addEventListener('mouseenter', () => {
            backButtonLogin.style.background = '#588ae7';
            backButtonLogin.style.padding = '2% 10% 2% 10%'
            backButtonLogin.style.margin = '2vh 0vh 0vh 18.5vh'

        })

        backButtonLogin.addEventListener('mouseleave', () => {
            backButtonLogin.style.background = '#284f96';
            backButtonLogin.style.padding = '2%';
            backButtonLogin.style.margin = '2vh 0vh 0vh 23.5vh'

        })

        backButtonLogin.addEventListener('click', () => {
            clearDiv(cardIndexDiv)
            showIndexScreen()
        })
    }, timeout);


}

