
const getUserDataGoogleAuth = async () => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    await fetch(`${backendURL}/user/showDataUser`, requestOptions)
        .then(res => res.json())
        .then(response => resp = response)
        .catch(error => console.log('error', error));
    return resp;
}



const fetchGetUserDataGoogleAuth = async () => {
    let data = window.location.href;
    data = data.replace(`${frontURL}views/googleRedirect.html?data=`, '')
    let sessionIDIndex = data.indexOf('sessionID');


    let sessionID = data.substring(sessionIDIndex);


    const sessionIDNotCleaned = data.substring(sessionIDIndex);


    sessionID = parseInt(sessionID.replace('sessionID=', ''));


    let token = data.replace(sessionIDNotCleaned, '');


    token = token.replace('&&', '');


    token = token.replace('token=', '');


    sessionStorage.setItem('sessionID', sessionID)


    
    sessionStorage.setItem('token', token)
    const userData = await getUserDataGoogleAuth();
    window.sessionStorage.setItem('userImg', JSON.stringify(userData.userImg))

    window.location.href = `${frontURL}views/home.html`;
}

fetchGetUserDataGoogleAuth()