validateUserToken('../../index.html');

let addressSelected;
const dataOrderFinal = {};


const cleanIDPay = (payData, payMethod) => {
    try {
        const payID = (payData.filter(pay => pay.name == payMethod))[0];
        if (payID == undefined) {
            return {}
        } else {
            return payID
        }
    } catch (error) {
        console.log(error);
        return {}
    }
}


const showPopUpSelectPayMethod = (payData,productData) => {



    const token = sessionStorage.getItem('token');



    const popUp2 = html('div', {
        width: '110vh',
        height: '100vh',
        margin: `250vh 0vh 0vh 50vh`,

    }, {
        class: 'popUp',
        id: 'popUp',
    });



    const paypalForm = html('form', {
    }, {
        action: `${backendURL}/paypal`,
        method: 'POST'
    }, popUp2)


    // const paypalForm = html('form', {
    // }, {
    //     action: `${backendURL}/paypalTest`,
    //     method: 'POST'
    // }, popUp2)




    html('input', {
    }, {
        value: token,
        type: 'hidden',
        name: 'token'
    }, paypalForm)


    html('input', {
    }, {
        value: addressSelected,
        type: 'hidden',
        name: 'addressIndex'
    }, paypalForm)

    let paypalMethodID = cleanIDPay(payData, 'Paypal')

    html('input', {
    }, {
        value: paypalMethodID._id,
        type: 'hidden',
        name: 'payID'
    }, paypalForm)



    const selectPaypal = html('input', {
        margin: `15vh 0vh 0vh 16vh`,
    }, {
        value: "PAGAR CON PAYPAL",
        class: 'selectPayMethod',
        type: 'submit',
        id: 'selectPaypal',
    }, paypalForm)

    selectPaypal.addEventListener('click', () => {
        dataOrderFinal.payName = 'Paypal';
    })





    let cashMethodID = cleanIDPay(payData, 'Efectivo')
    const cashInfoOrder ={
        payID : cashMethodID,
        addressIndex : addressSelected,
        arrFood : productData
    };



    const selectCash = html('button', {
        margin: `30vh 0vh 0vh 16vh`,
    }, {
        textContent: "PAGAR EN EFECITVO",
        class: 'selectPayMethod',
        id: 'selectCash',
    }, popUp2)

    selectCash.addEventListener('click', async()=>{
        const response = await sendOrderData(cashInfoOrder);
        if(response.status == 200){
            window.location.href = `${frontUrl}views/shop/orderConfirmed.html`;
        }else{
            console.error(response.msj);
        }
    })



    let cancelButtonAux = document.getElementById('popUp').style.height;
    cancelButtonAux = parseInt(cancelButtonAux.replace('vh', ''));
    cancelButtonAux -= 20

    const cancelButton = html('button', {
        margin: `${cancelButtonAux}vh 0vh 0vh 27vh`,
    }, {
        textContent: "CANCELAR",
        class: 'selectPayMethod',
        id: 'cancelButton',
    }, popUp2)

    cancelButton.addEventListener('click', () => {
        disablePopUpOrder()
    })


    // const confirmationPopUpTitle = html('h3', {
    // }, {
    //     id: 'confirmationPopUpTitle',
    //     class: 'titleConfirmationPopUp',
    // }, confirmationPopUpContainer);
    // confirmationPopUpTitle.innerText = `¿ESTA SEGURO/A DE ELIMINAR SU CUENTA? \n (ESTA ACCION NO SE PUEDE DESHACER)`,


    //     html('br', {}, {}, confirmationPopUpContainer)

    // const confirmationPopUpSaveButton = html('button', {
    //     color: "#45b24e",
    //     backgroundColor: '#285e2d',
    //     margin: '5vh 0vh 0vh 3vh ',
    // }, {
    //     id: 'confirmationPopUpSaveButton',
    //     class: 'actionButtonsConfirmationPopUp',
    //     textContent: 'ACEPTAR',
    // }, confirmationPopUpContainer);


    // confirmationPopUpSaveButton.addEventListener('mouseenter', () => {
    //     confirmationPopUpSaveButton.style.backgroundColor = '#45b24e';
    //     confirmationPopUpSaveButton.style.color = '#285e2d';
    // })


    // confirmationPopUpSaveButton.addEventListener('mouseleave', () => {
    //     confirmationPopUpSaveButton.style.backgroundColor = '#285e2d';
    //     confirmationPopUpSaveButton.style.color = '#45b24e';
    // })

    // confirmationPopUpSaveButton.addEventListener('click', () => {
    //     deleteMyAccount();
    // })


    // const confirmationPopUpCancelButton = html('button', {
    //     color: "#c43b3b",
    //     backgroundColor: '#742929',
    //     margin: '5vh 0vh 0vh 3vh ',
    // }, {
    //     id: 'confirmationPopUpCancelButton',
    //     class: 'actionButtonsConfirmationPopUp',
    //     textContent: 'CANCELAR',
    // }, confirmationPopUpContainer);

    // confirmationPopUpCancelButton.addEventListener('mouseenter', () => {
    //     confirmationPopUpCancelButton.style.backgroundColor = '#c43b3b';
    //     confirmationPopUpCancelButton.style.color = '#742929';
    // })


    // confirmationPopUpCancelButton.addEventListener('mouseleave', () => {
    //     confirmationPopUpCancelButton.style.backgroundColor = '#742929';
    //     confirmationPopUpCancelButton.style.color = '#c43b3b';
    // })

    // confirmationPopUpCancelButton.addEventListener('click', () => {
    //     confirmationPopUpContainer.remove()
    // })


}



const showPopUpMsj = (msj, margin, popUp2) => {

    const showMSJPopUpContainer = html('div', {

        margin: margin,

    }, {
        class: 'containerConfirmationPopUp',
        id: 'showMSJPopUpContainer'
    }, popUp2);


    const showMSJPopUpTitle = html('h3', {
    }, {
        id: 'showMSJPopUpTitle',
        class: 'titleConfirmationPopUp',
        textContent: msj,

    }, showMSJPopUpContainer);



    html('br', {}, {}, showMSJPopUpContainer)

    const showMSJPopUpSaveButton = html('button', {
        color: "#45b24e",
        backgroundColor: '#285e2d',
        margin: '5vh 0vh 0vh 3vh ',
    }, {
        id: 'showMSJPopUpSaveButton',
        class: 'actionButtonsConfirmationPopUp',
        textContent: 'ACEPTAR',
    }, showMSJPopUpContainer);


    showMSJPopUpSaveButton.addEventListener('mouseenter', () => {
        showMSJPopUpSaveButton.style.backgroundColor = '#45b24e';
        showMSJPopUpSaveButton.style.color = '#285e2d';
    })


    showMSJPopUpSaveButton.addEventListener('mouseleave', () => {
        showMSJPopUpSaveButton.style.backgroundColor = '#285e2d';
        showMSJPopUpSaveButton.style.color = '#45b24e';
    })

    showMSJPopUpSaveButton.addEventListener('click', () => {
        document.getElementById('showMSJPopUpContainer').remove()
    })
}




// const order = {
//     arrFood: products,
//     address: addressSelected.index + 1,
//     idPay: idPay._id,
//     totalAmmout: `${(totalAmmout / process.env.REACT_APP_DOLLAR).toFixed(2)}`,
//     idsProducts: idsProducts

const showMyOrderData = (userData, productData, totalImport, payData) => {


    addressSelected = 0 + 1;

    dataOrderFinal.totalImport = totalImport;
    dataOrderFinal.arrFood = productData;
    dataOrderFinal.address = addressSelected;
    // payName: payName._id;



    const UIfirstLayer = html('div', {
        transition: 'background-color 0.3s,opacity 0.3s',
    }, {
        id: 'UIfirstLayer',
    })


    const userDataDivOrder = html('div', {
        transition: 'background-color 0.3s,opacity 0.3s'
    }, {
        id: 'userDataDivOrder',
    }, UIfirstLayer);


    html('h3', {
        margin: '50vh 0vh 0vh 60vh',
    }, {
        textContent: `NOMBRE DE USUARIO: ${userData.username}`,
        class: 'textElementDoOrder',
    }, userDataDivOrder)

    html('h3', {
        margin: '80vh 0vh 0vh 60vh',
    }, {
        textContent: `NOMBRE DE USUARIO: ${userData.name}`,
        class: 'textElementDoOrder',
    }, userDataDivOrder)

    html('h3', {
        margin: '110vh 0vh 0vh 60vh',
    }, {
        textContent: `NOMBRE DE USUARIO: ${userData.email}`,
        class: 'textElementDoOrder',
    }, userDataDivOrder)

    html('h3', {
        margin: '140vh 0vh 0vh 60vh',
    }, {
        textContent: `NOMBRE DE USUARIO: ${userData.phone}`,
        class: 'textElementDoOrder',
    }, userDataDivOrder)

    const dropdownAddress = html('div', {}, { class: 'dropdownAddress' }, userDataDivOrder)
    const dropButtonAddress = html('button', {}, { id: 'dropButtonAddress', class: 'dropButtonAddress', textContent: 'AGENDA DE DIRECCIONES' }, dropdownAddress)
    const dropdownAddressContent = html('div', {}, { id: 'dropdownAddressContent', class: 'dropdownAddressContent' }, dropButtonAddress)

    for (let i = 0; i < userData.address.length; i++) {
        const addressButton = html('button', {}, { textContent: userData.address[i] }, dropdownAddressContent)
        addressButton.addEventListener('click', () => {
            addressSelected = i + 1;
            dataOrderFinal.address = addressSelected;
        })

    }

    html('h3', {
        margin: '200vh 0vh 0vh 90vh',
    }, {
        textContent: "COMIDA",
        class: 'textElementDoOrderNoHover',
    }, userDataDivOrder)

    html('h3', {
        margin: '210vh 0vh 0vh 60vh',
        color: '#588ae7',
    }, {
        textContent: "---------------------------------------------",
        class: 'textElementDoOrderNoHover',
    }, userDataDivOrder)


    for (let i = 0; i < productData.length; i++) {

        html('h3', {
            margin: `${220 + (60 * i)}vh 0vh 0vh 60vh`,
        }, {
            textContent: `PRODUCTO: ${productData[i].name}`,
            class: 'textElementDoOrder',
        }, userDataDivOrder)

        html('h3', {
            margin: `${240 + (60 * i)}vh 0vh 0vh 60vh`,
        }, {
            textContent: `PRECIO UNITARIO: $${productData[i].price}`,
            class: 'textElementDoOrder',
        }, userDataDivOrder)

        html('h3', {
            margin: `${260 + (60 * i)}vh 0vh 0vh 60vh`,
        }, {
            textContent: `CANTIDAD: ${productData[i].ammount} UNIDAD/ES`,
            class: 'textElementDoOrder',
        }, userDataDivOrder)

        html('h3', {
            margin: `${270 + (60 * i)}vh 0vh 0vh 60vh`,
            color: '#588ae7',
        }, {
            textContent: "---------------------------------------------",
            class: 'textElementDoOrderNoHover',
        }, userDataDivOrder)

        if ((productData.length - 1) == i) {
            html('h3', {
                margin: `${280 + (60 * i)}vh 0vh 0vh 60vh`,
            }, {
                textContent: `IMPORTE TOTAL : $${totalImport}`,
                class: 'textElementDoOrder',
                id: 'totalImport',
            }, userDataDivOrder)

            html('h3', {
                margin: `${290 + (60 * i)}vh 0vh 0vh 60vh`,
                color: '#588ae7',
            }, {
                textContent: "---------------------------------------------",
                class: 'textElementDoOrderNoHover',
                id: 'totalImportDivider',
            }, userDataDivOrder)

        }

    }


    let selectPayMethodMarginAux = document.getElementById('totalImportDivider').style.marginTop;
    selectPayMethodMarginAux = parseInt(selectPayMethodMarginAux.replace('vh', ''));
    selectPayMethodMarginAux += 20

    const selectPayMethod = html('button', {
        margin: `${selectPayMethodMarginAux}vh 0vh 0vh 60vh`,
    }, {
        textContent: "CONFIMAR PEDIDO",
        class: 'selectPayMethod',
        id: 'selectPayMethod',
    }, userDataDivOrder)

    selectPayMethod.addEventListener('click', () => {
        showPopUpSelectPayMethod(payData,productData)
    })







    html('div', {
        display: 'inline',
        position: 'absolute',
        backgroundColor: 'transparent',
        margin: `${selectPayMethodMarginAux}vh 0vh 0vh 0vh`,
        height: '40vh',
        width: '40vh',
        zIndex: 1000
    }, {
        class: 'spacer',
        id: 'spacer',
    }, UIfirstLayer)



    dropButtonAddress.addEventListener('click', (event) => {
        openCloseAddressDropdown(event)
        closePopUpPayMethod(event)
    })


    UIfirstLayer.onclick = function (event) {
        autoCloseAddressDropdown(event)
        closePopUpPayMethod(event)
    }


}

const autoCloseAddressDropdown = (event) => {
    if (!event.target.matches('.dropButtonAddress')) {
        const dropdowns = document.getElementsByClassName("dropdownAddressContent");
        for (let i = 0; i < dropdowns.length; i++) {
            const openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('showBannerAddress')) {
                openDropdown.classList.remove('showBannerAddress');
            }
        }
    }
}


const openCloseAddressDropdown = (e) => {
    if (e.target.matches('.dropButtonAddress')) {
        const dropdowns = document.getElementsByClassName("dropdownAddressContent");
        for (let i = 0; i < dropdowns.length; i++) {
            const openDropdown = dropdowns[i];
            document.getElementById("dropdownAddressContent").classList.toggle("showBannerAddress");
        }
    }
}

const closePopUpPayMethod = (event) => {
    if (!event.target.matches('#selectPayMethod')) {
        disablePopUpOrder()
    }
}