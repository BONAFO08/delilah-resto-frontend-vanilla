
const disablePopUpOrder = () => {
    try {
        document.getElementById('popUp').remove()
        document.body.style.overflow = "";
    } catch (error) {

    }
}


const showPopUpConfirmationCancelOrder = (address, index) => {

    const popUp2 = html('div', {


    }, {
        class: 'popUp',
        id: 'popUp'
    });



    const confirmationPopUpContainer = html('div', {

        margin: '10vh 0vh 0vh 30vh',

    }, {
        class: 'containerConfirmationPopUp',
        id: 'confirmationPopUpContainer'
    }, popUp2);


    const confirmationPopUpTitle = html('h3', {
    }, {
        id: 'confirmationPopUpTitle',
        class: 'titleConfirmationPopUp',
    }, confirmationPopUpContainer);
    confirmationPopUpTitle.innerHTML = `¿ESTA SEGURO/A DE ELIMINAR <span style="color:#b46d3b">${address.toUpperCase()}</span> DE SU AGENDA DE  DIRECCIONES?`,


        html('br', {}, {}, confirmationPopUpContainer)

    const confirmationPopUpSaveButton = html('button', {
        color: "#45b24e",
        backgroundColor: '#285e2d',
        margin: '5vh 0vh 0vh 3vh ',
    }, {
        id: 'confirmationPopUpSaveButton',
        class: 'actionButtonsConfirmationPopUp',
        textContent: 'ACEPTAR',
    }, confirmationPopUpContainer);


    confirmationPopUpSaveButton.addEventListener('mouseenter', () => {
        confirmationPopUpSaveButton.style.backgroundColor = '#45b24e';
        confirmationPopUpSaveButton.style.color = '#285e2d';
    })


    confirmationPopUpSaveButton.addEventListener('mouseleave', () => {
        confirmationPopUpSaveButton.style.backgroundColor = '#285e2d';
        confirmationPopUpSaveButton.style.color = '#45b24e';
    })

    confirmationPopUpSaveButton.addEventListener('click', () => {
        deleteAddress(index, popUp2)


    })


    const confirmationPopUpCancelButton = html('button', {
        color: "#c43b3b",
        backgroundColor: '#742929',
        margin: '5vh 0vh 0vh 3vh ',
    }, {
        id: 'confirmationPopUpCancelButton',
        class: 'actionButtonsConfirmationPopUp',
        textContent: 'CANCELAR',
    }, confirmationPopUpContainer);

    confirmationPopUpCancelButton.addEventListener('mouseenter', () => {
        confirmationPopUpCancelButton.style.backgroundColor = '#c43b3b';
        confirmationPopUpCancelButton.style.color = '#742929';
    })


    confirmationPopUpCancelButton.addEventListener('mouseleave', () => {
        confirmationPopUpCancelButton.style.backgroundColor = '#742929';
        confirmationPopUpCancelButton.style.color = '#c43b3b';
    })

    confirmationPopUpCancelButton.addEventListener('click', () => {
        confirmationPopUpContainer.remove()
    })


}



const showPopUpMsjDoOrder = (msj, margin) => {



    const popUp2 = html('div', {


    }, {
        class: 'popUp',
        id: 'popUp'
    });



    const showMSJPopUpContainer = html('div', {

        margin: margin,

    }, {
        class: 'containerConfirmationPopUp',
        id: 'showMSJPopUpContainer'
    }, popUp2);


    const showMSJPopUpTitle = html('h3', {
    }, {
        id: 'showMSJPopUpTitle',
        class: 'titleConfirmationPopUp',
        textContent: msj,

    }, showMSJPopUpContainer);



    html('br', {}, {}, showMSJPopUpContainer)

    const showMSJPopUpSaveButton = html('button', {
        color: "#45b24e",
        backgroundColor: '#285e2d',
        margin: '5vh 0vh 0vh 3vh ',
    }, {
        id: 'showMSJPopUpSaveButton',
        class: 'actionButtonsConfirmationPopUp',
        textContent: 'ACEPTAR',
    }, showMSJPopUpContainer);


    showMSJPopUpSaveButton.addEventListener('mouseenter', () => {
        showMSJPopUpSaveButton.style.backgroundColor = '#45b24e';
        showMSJPopUpSaveButton.style.color = '#285e2d';
    })


    showMSJPopUpSaveButton.addEventListener('mouseleave', () => {
        showMSJPopUpSaveButton.style.backgroundColor = '#285e2d';
        showMSJPopUpSaveButton.style.color = '#45b24e';
    })

    showMSJPopUpSaveButton.addEventListener('click', () => {
        document.getElementById('showMSJPopUpContainer').remove()
    })
}

const showPopUpMsjDoOrderWithRelocation = (msj, margin,newLocation) => {



    const popUp2 = html('div', {


    }, {
        class: 'popUp',
        id: 'popUp'
    });



    const showMSJPopUpContainer = html('div', {

        margin: margin,

    }, {
        class: 'containerConfirmationPopUp',
        id: 'showMSJPopUpContainer'
    }, popUp2);


    const showMSJPopUpTitle = html('h3', {
    }, {
        id: 'showMSJPopUpTitle',
        class: 'titleConfirmationPopUp',
        textContent: msj,

    }, showMSJPopUpContainer);



    html('br', {}, {}, showMSJPopUpContainer)

    const showMSJPopUpSaveButton = html('button', {
        color: "#45b24e",
        backgroundColor: '#285e2d',
        margin: '5vh 0vh 0vh 3vh ',
    }, {
        id: 'showMSJPopUpSaveButton',
        class: 'actionButtonsConfirmationPopUp',
        textContent: 'ACEPTAR',
    }, showMSJPopUpContainer);


    showMSJPopUpSaveButton.addEventListener('mouseenter', () => {
        showMSJPopUpSaveButton.style.backgroundColor = '#45b24e';
        showMSJPopUpSaveButton.style.color = '#285e2d';
    })


    showMSJPopUpSaveButton.addEventListener('mouseleave', () => {
        showMSJPopUpSaveButton.style.backgroundColor = '#285e2d';
        showMSJPopUpSaveButton.style.color = '#45b24e';
    })

    showMSJPopUpSaveButton.addEventListener('click', () => {
        document.getElementById('showMSJPopUpContainer').remove()
        window.location.href = newLocation;
    })
}
