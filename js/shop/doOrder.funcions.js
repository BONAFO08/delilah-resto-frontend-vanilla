
const getProductsDoOrder = async () => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    await fetch(`${backendURL}/user/products`, requestOptions)
        .then(res => res.json())
        .then(response => resp = response)
        .catch(error => console.error('error', error));
    return resp;
}


const getUserDataDoOrder = async () => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    await fetch(`${backendURL}/user/showDataUser`, requestOptions)
        .then(res => res.json())
        .then(response => resp = response)
        .catch(error => console.error('error', error));
    return resp;
}



const sendOrderData = async (orderData) => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

    let requestOptions = {
        method: 'POST',
        body: JSON.stringify(orderData),
        headers: myHeaders,
        redirect: 'follow'
    };

    await fetch(`${backendURL}/newOrder/`, requestOptions)
        .then(response => resp = response.json())
        .catch(error => console.log('error', error));
    return resp;
}


const getPayDataDoOrder = async () => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    await fetch(`${backendURL}/user/payments`, requestOptions)
        .then(res => res.json())
        .then(response => resp = response)
        .catch(error => console.error('error', error));
    return resp;
}



const validateShopCart = async () => {

    const cart = sessionStorage.getItem('cart')
    if (cart == null || cart == '[]') {
        const location = `${frontURL}${productsURL}`; 
        showPopUpMsjDoOrderWithRelocation('Carrito vacio'.toUpperCase(), '30vh 0vh 0vh 50vh', location)
    } else {
        const productData = await getProductsDoOrder();
        const userData = await getUserDataDoOrder();
        const payData = await getPayDataDoOrder();

        const data = {
            cart: JSON.parse(cart),
            userData: userData,
            productData: productData,
            payData: payData
        };
        return data
    }
}


validateShopCart()
    .then(resolve => showOrderData(resolve))
    .catch(error => console.error(error));

const showOrderData = (resolve) => {
    const productData = resolve.productData;
    const userData = resolve.userData;
    const payData = resolve.payData;
    const cartData = resolve.cart;


    const validatedFood = validateFood(productData, cartData)
    validateUserData(userData)
    const totalImport = calculateTotalImport(validatedFood);
    showMyOrderData(userData, validatedFood, totalImport, payData)


}


const validateFood = (productData, cartData) => {
    const validatedFood = cartData.map(cartProduct => {
        const originalProductData = (productData.filter(productData => productData._id == cartProduct.id))[0];
        if (originalProductData != undefined) {
            originalProductData.ammount = cartProduct.ammount;
        }
        return originalProductData
    });

    if (validatedFood.length == 0) {
        const location = `${frontURL}${productsURL}`; 
        showPopUpMsjDoOrderWithRelocation('Carrito vacio'.toUpperCase(), '30vh 0vh 0vh 50vh', location)
        sessionStorage.setItem('cart', [])
    } else {
        const cleanedFood = validatedFood.map(product => {
           return {
            ammount : product.ammount,
            name : product.name,
            price : product.price,
            _id : product._id ,
        }
        });
        return cleanedFood;
    }
}


const validateUserData = (userData) => {
    const location = `${frontURL}${myAccountURL}`; 
    if (userData.address.length == 0) {
        showPopUpMsjDoOrderWithRelocation('Agenda Vacia'.toUpperCase(), '30vh 0vh 0vh 50vh', location)
    } else if (userData.phone == 0 || isNaN(userData.phone) == true) {
        showPopUpMsjDoOrderWithRelocation('Numero telefonico invalido'.toUpperCase(), '30vh 0vh 0vh 50vh', location)
    }
}

const calculateTotalImport = (validatedFood) => {


    const totalImport = validatedFood.reduce((previousValue, currentValue) =>
        previousValue + (currentValue.price * currentValue.ammount), 0);
    return totalImport
}


