 validateUserToken('../../index.html');

const showProducts = async () => {
    const productData = await getProducts()
 
    for (let i = 0; i < productData.length; i++) {
        createProductCard(productData[i], i, productData.length)
    }
}

const cartList = JSON.parse(sessionStorage.getItem('cart'));


const createProductCard = (product, index, endOfTheList) => {
    const productContainer = html('div',
        {
            position: 'absolute',
            width: "fit-content",
            height: "fit-content",
            margin: `${(30 * (index + 1)) + (30 * index)}vh 0vh 0vh 60vh`,
            backgroundColor: '#000000',
            border: "5px solid #000000 ",
            borderRadius: "10%",
            fontFamily: "'Josefin Sans', sans-serif",
            transition: "all 1.5s",
            textShadow: '2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000',
            color: '#bdc4ff',

        },


        {
            id: `${product._id}Container`,
            class: 'productContainer'
        });



    productContainer.addEventListener('mouseenter', () => {
        onEnterProductContainer(product._id, index)

    })


    productContainer.addEventListener('mouseleave', () => {
        onLeaveProductContainer(product._id, index)

    })




    const productImg = html('img',
        {
            display: 'inline',
            width: '70vh',
            height: '45vh',
            margin: "0vh 0vh 0vh 0vh",
            borderRadius: '10%',
            border: "5px solid #000000 ",
        },
        {
            id: `${product._id}Img`,
            src: product.imgUrl
        },
        productContainer);

    const productName = html('h3',
        {
            display: 'none',
            position: "absolute",
            fontSize: "40px",
            margin: "0vh 0vh 0vh 25vh"
        },
        {
            id: `${product._id}Name`,
            textContent: product.name.toUpperCase()

        },
        productContainer);

    const ingredientsName = html('h3',
        {
            display: 'none',
            position: "absolute",
            fontSize: "40px",
            margin: "10vh 0vh 0vh 10vh"
        },
        {
            id: `${product._id}Ingredients`,
            textContent: `INGREDIENTES: ${product.ingredients.toUpperCase()}`

        },
        productContainer);

    const priceName = html('h3',
        {
            display: 'none',
            position: "absolute",
            fontSize: "40px",
            margin: "30vh 0vh 0vh 10vh"
        },
        {
            id: `${product._id}Price`,
            textContent: `PRECIO: $${product.price}`

        },
        productContainer);


    const shopButton = html('button',
        {
            display: 'none',
            position: "absolute",
            fontSize: "40px",
            color: "#aab0ff",
            margin: "38vh 0vh 0vh 30vh",
            cursor: 'pointer',
            borderRadius: "25%",
            padding: '2vh',
            backgroundColor: '#1b3e98',
            textShadow: '2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000',
            color: '#bdc4ff',
            transition: 'all 0.5s'
        },
        {
            id: `${product._id}Shop`,
            textContent: `AGREGAR AL CARRITO`

        },
        productContainer);


    shopButton.addEventListener('mouseenter', () => {
        shopButton.style.backgroundColor = "#aab0ff";
        shopButton.style.color = '#2555ce';
        shopButton.style.padding = '2vh 6vh';
    })


    shopButton.addEventListener('mouseleave', () => {
        shopButton.style.backgroundColor = '#1b3e98';
        shopButton.style.color = "#aab0ff";
        shopButton.style.padding = '2vh';
    })


    shopButton.addEventListener('click', () => {
        addToCart(product)
        hiddeProductShopButton(shopButton)
        document.getElementsByClassName('dropButtonShopCart')[0].style.transition = "background-color 0.5s";
        document.getElementsByClassName('dropButtonShopCart')[0].style.backgroundColor = "#dfb169";
        updateAmmountCounter()
        setTimeout(() => {
            document.getElementsByClassName('dropButtonShopCart')[0].style.backgroundColor = "#3498DB";
        }, 1000);
    })



    if (cartList != null) {
        const productIsInList = cartList.filter(cartList => cartList.id == product._id)
        if (productIsInList.length != 0) {
            hiddeProductShopButton(shopButton)
        }
    }



    if (endOfTheList == index + 1) {
        html('div',
            {
                display: 'inline',
                position: "absolute",
                padding: '20vh 0vh 0vh 0vh',
                border: '2px solid transparent',
                margin: `${(30 * (index + 3)) + (30 * index + 1)}vh 0vh 0vh 60vh`,
            });
    }
}


const onEnterProductContainer = (_id, index) => {
    const containerStyle = document.getElementById(`${_id}Container`).style;

    containerStyle.width = "180vh";
    containerStyle.height = "50vh";
    containerStyle.padding = "5vh 0vh 0vh 0vh";

    containerStyle.margin = `${(30 * (index + 1)) + (30 * index)}vh 0vh 0vh 0vh`;
    containerStyle.backgroundColor = "#7179ea";

    const nameStyle = document.getElementById(`${_id}Name`).style;
    nameStyle.display = 'inline';

    const ingredientsStyle = document.getElementById(`${_id}Ingredients`).style;
    ingredientsStyle.display = 'inline';

    const priceStyle = document.getElementById(`${_id}Price`).style;
    priceStyle.display = 'inline';

    const shopStyle = document.getElementById(`${_id}Shop`).style;
    shopStyle.display = 'inline';

    const imgStyle = document.getElementById(`${_id}Img`).style;
    imgStyle.margin = "0vh 0vh 0vh 0.5vh";

}

const onLeaveProductContainer = (_id, index) => {

    const containerStyle = document.getElementById(`${_id}Container`).style;

    containerStyle.width = "fit-content";
    containerStyle.height = "fit-content";
    containerStyle.margin = `${(30 * (index + 1)) + (30 * index)}vh 0vh 0vh 60vh`;
    containerStyle.backgroundColor = "#000000";
    containerStyle.padding = "0vh 0vh 0vh 0vh";


    const nameStyle = document.getElementById(`${_id}Name`).style;
    nameStyle.display = 'none';

    const ingredientsStyle = document.getElementById(`${_id}Ingredients`).style;
    ingredientsStyle.display = 'none';

    const priceStyle = document.getElementById(`${_id}Price`).style;
    priceStyle.display = 'none';

    const shopStyle = document.getElementById(`${_id}Shop`).style;
    shopStyle.display = 'none';

    const imgStyle = document.getElementById(`${_id}Img`).style;
    imgStyle.margin = "0vh 0vh 0vh 0vh";
}

showProducts()

//>
setCartImg('../../imgs/shopCart.png')
//>
