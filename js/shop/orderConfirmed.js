validateUserToken('../../index.html');


const deleteOrderConfirmed = async () => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

    let requestOptions = {
        method: 'DELETE',
        headers: myHeaders,
        redirect: 'follow'
    };

    console.log(backendURL);
    console.log(window.sessionStorage.getItem('token'));


    await fetch(`${backendURL}/orderCollector/deleteOrder`, requestOptions)
        .then(res => res.json())
        .then(response => {
             console.log(resp)
             return resp = response
            })
        .catch(error => console.log('error', error));
    return resp;
}

deleteOrderConfirmed();

showPopUpMsjDoOrderWithRelocation(
    'Orden Confirmada! Puede revisar el estado de su pedido en el apartado de MIS "PEDIDOS"'.toUpperCase(),
    '30vh 0vh 0vh 50vh',
    `${frontURL}${homeURL}`
)



sessionStorage.setItem('cart', '[]')
