validateUserToken('../../index.html');
setCartImg('../../imgs/shopCart.png');

let productsData = [];
let originalDataProducts = [];
let selectedProductIsShowing = false;
let actualProductShowing = undefined;


// Product Data Fech
const getProductsManager = async () => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    await fetch(`${backendURL}/user/products`, requestOptions)
        .then(res => res.json())
        .then(response => resp = response)
        .catch(error => console.log('error', error));
    return resp;
}


//Fetch the data and save in productsData (data about the products) and originalDataProducts 
//(the original data without changes) 
const showProductsManager = async () => {
    productsData = await getProductsManager()
    originalDataProducts = JSON.stringify(productsData);
    originalDataProducts = JSON.parse(originalDataProducts);

    // console.log(JSON.stringify(productsData) == JSON.stringify(originalDataProducts));

    console.log(productsData);

    const productManagerMasterContainerDIV = html('div',
        {
            textAlign: "center",

        }, {
        id: `productManagerMasterContainerDIV`,
    },
    )


    showGeneralButtons(productManagerMasterContainerDIV)
    for (let i = 0; i < productsData.length; i++) {
        createPoductCardManager(productsData[i], i, productsData.length, productManagerMasterContainerDIV)
    }
}




//Create :
// Button for create a product
// Button for send all changes
// Button for discard all changes
const showGeneralButtons = (productManagerMasterContainerDIV) => {

    const productsNewProductButton = html('button', {
        margin: "40vh 0vh 0vh 0vh",
    }, {
        textContent: 'CREAR PRODUCTO',
        id: 'productsNewProductButton',
        class: 'productSelectedElement  productsButtons'
    },
        productManagerMasterContainerDIV)

    html('br', {}, {}, productManagerMasterContainerDIV)
    html('br', {}, {}, productManagerMasterContainerDIV)
    html('br', {}, {}, productManagerMasterContainerDIV)

    const productsConfirmButton = html('button', {
    }, {
        textContent: 'ENVIAR TODOS LOS  CAMBIOS',
        id: 'productsConfirmButton',
        class: 'productSelectedElement  productsButtons'
    },
        productManagerMasterContainerDIV)


    html('br', {}, {}, productManagerMasterContainerDIV)
    html('br', {}, {}, productManagerMasterContainerDIV)
    html('br', {}, {}, productManagerMasterContainerDIV)

    const productsCancelateButton = html('button', {
    }, {
        textContent: 'DESCARTAR TODOS LOS CAMBIOS',
        id: 'productsCancelateButton',
        class: 'productSelectedElement  productsButtons'
    },
        productManagerMasterContainerDIV)



        productsCancelateButton.addEventListener('click',()=>{
            discardAllChange()
        })
}





showProductsManager()

