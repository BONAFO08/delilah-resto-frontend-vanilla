
// Enable the hover propiety in a div
const turnONhoverProductDiv = (productID) => {

    document.getElementById(`${productID}ContainerManager`).onmouseenter = () => {
        document.getElementById(`${productID}ContainerManager`).style.color = "#284f96";
        document.getElementById(`${productID}ContainerManager`).style.backgroundColor = "#588ae7";
        document.getElementById(`${productID}ContainerManager`).style.border = "5px solid #365286";
    }


    document.getElementById(`${productID}ContainerManager`).onmouseleave = () => {
        document.getElementById(`${productID}ContainerManager`).style.color = "#588ae7";
        document.getElementById(`${productID}ContainerManager`).style.backgroundColor = "#284f96";
        document.getElementById(`${productID}ContainerManager`).style.border = "5px solid #1d3767";
    }



}

// Disable the hover propiety in a div
const turnOFFhoverProductDiv = (productID) => {
    document.getElementById(`${productID}ContainerManager`).onmouseenter = undefined;
    document.getElementById(`${productID}ContainerManager`).onmouseleave = undefined;
}

// Create the Div container of a Product
const createPoductCardManager = (product, index, endOfTheList, productManagerMasterContainerDIV) => {
    const productContainerManager = html('div',
        {
            margin: `${(30 * (index + 1)) + (-10 * index)}vh 0vh 0vh 50vh`,
            cursor: 'pointer',

        }, {
        id: `${product._id}ContainerManager`,
        class: 'productContainerManager productDesk',
    },
        productManagerMasterContainerDIV)

    productContainerManager.originalMargin = productContainerManager.style.margin;
    productContainerManager.originalHeight = productContainerManager.style.height;

    productContainerManager.addEventListener('click', (e) => {
        if (e.target.className.includes('productDesk')) {
            openInfoProductDiv(product);
            managerDivClick(product)
        }
    })

    const productNameManager = html('h3',
        {
            textAlign: "center",
            fontSize: "40px",
            cursor: 'pointer',
        },
        {
            id: `${product._id}Name`,
            textContent: product.name.toUpperCase(),
            class: 'productDesk'
        },
        productContainerManager);

    turnONhoverProductDiv(product._id)


    if (endOfTheList == index + 1) {
        html('div',
            {
                display: 'inline',
                position: "absolute",
                padding: '5vh 0vh 0vh 0vh',
                border: '2px solid transparent',
                margin: `${(30 * (index + 1))}vh 0vh 0vh 60vh`,
            });
    }

}

//Open the selected Div and back the CSS atributes of the others divs 
const openInfoProductDiv = (product) => {
    const productIndex = productsData.indexOf(product);
    let verificatorDivIsOpen = false;
    hiddeInfoProductDiv(product)

    productsData.map(product => {
        const htmlElement = document.getElementById(`${product._id}ContainerManager`)
        htmlElement.style.margin = htmlElement.originalMargin;
        htmlElement.style.height = htmlElement.originalHeight;
        htmlElement.style.color = "#588ae7",
            htmlElement.style.backgroundColor = "#284f96",
            htmlElement.style.border = "5px solid #1d3767"
        turnONhoverProductDiv(product._id)

    })

    if (actualProductShowing != undefined) {
        (actualProductShowing._id != product._id)
            ? (selectedProductIsShowing = false)
            : (selectedProductIsShowing = true);
    }

    for (let i = productIndex; i < productsData.length; i++) {


        if (productsData[i] != undefined && selectedProductIsShowing == false) {
            const baseAddToDiv = 200;
            if (productsData[i + 1] != undefined) {
                const htmlElement = document.getElementById(`${productsData[i + 1]._id}ContainerManager`);
                const elementMargin = parseInt((htmlElement.style.marginTop).replace('vh', ''));
                htmlElement.style.marginTop = `${elementMargin + baseAddToDiv}vh`
                htmlElement.style.height = htmlElement.originalHeight;
                document.getElementById(`${product._id}ContainerManager`).style.height = `${baseAddToDiv + 10}vh`
                verificatorDivIsOpen = true;
            } else {
                document.getElementById(`${product._id}ContainerManager`).style.height = `${baseAddToDiv + 10}vh`
                verificatorDivIsOpen = true;
            }

        } else if (productsData[i + 1] != undefined && selectedProductIsShowing == true && actualProductShowing == undefined) {
            const htmlElement = document.getElementById(`${productsData[i + 1]._id}ContainerManager`);
            htmlElement.style.margin = htmlElement.originalMargin;
            htmlElement.style.height = htmlElement.originalHeight;
            verificatorDivIsOpen = false;
        }
    }

    (verificatorDivIsOpen)
        ? (selectedProductIsShowing = true,
            actualProductShowing = product,
            turnOFFhoverProductDiv(product._id),
            showInfoProductDiv(product))
        : (selectedProductIsShowing = false,
            actualProductShowing = undefined);


}

// Show the information about the selected Product
const showInfoProductDiv = (product) => {
    const divContainerProduct = document.getElementById(`${product._id}ContainerManager`);

    const oldDataProductSelected = originalDataProducts.filter(food => food._id == product._id)[0];

    const dataDivContainer = html('div', { textAlign: 'center', }, { id: 'dataDivContainer', }, divContainerProduct)



    //PRODUCT NAME
    const productSelectedTextName = html('h3', {
        display: (oldDataProductSelected.name != product.name) ? ('none') : ('block'),
    }, {
        textContent: "Nombre: " + oldDataProductSelected.name,
        id: 'productSelectedTextName',
        class: 'productSelectedElement productSelectedText'
    },
        dataDivContainer)


    productSelectedTextName.addEventListener('click', () => {
        productSelectedCancelateIndividualPropietyButtonName.style.opacity = 1;
        productSelectedTextName.style.display = 'none'
        productSelectedInputName.style.display = 'block'
    })


    const productSelectedInputName = html('input', {
        display: (oldDataProductSelected.name != product.name) ? ('block') : ('none'),
        cursor: 'default',
    }, {
        placeholder: (oldDataProductSelected.name != product.name) ? (product.name) : ("NUEVO NOMBRE"),
        id: 'productSelectedInputName',
        class: 'productSelectedElement productSelectedInput',
        name : 'name'
    },
        dataDivContainer)


    const productSelectedCancelateIndividualPropietyButtonName = html('button', {
        opacity: (oldDataProductSelected.name != product.name) ? (1) : (0),
    }, {
        textContent: 'DESCARTAR CAMBIO',
        id: 'productSelectedCancelateIndividualPropietyButtonName',
        class: 'productSelectedElement cancelateIndividualPropietyButton'
    },
        dataDivContainer)

    productSelectedCancelateIndividualPropietyButtonName.addEventListener('click', () => {
        productSelectedCancelateIndividualPropietyButtonName.style.opacity = 0;
        productSelectedTextName.style.display = 'block'
        productSelectedInputName.value = ''
        productSelectedInputName.style.display = 'none'
        discardAChange(product._id, 'name')
    })



    // PRODUCT INGREDIENTS

    const productSelectedTextIngredients = html('h3', {
        display: (oldDataProductSelected.ingredients != product.ingredients) ? ('none') : ('block'),
    }, {
        textContent: "Ingredientes: " + oldDataProductSelected.ingredients,
        id: 'productSelectedTextIngredients',
        class: 'productSelectedElement productSelectedText'
    },
        dataDivContainer)


    productSelectedTextIngredients.addEventListener('click', () => {
        productSelectedCancelateIndividualPropietyButtonIngredients.style.opacity = 1;
        productSelectedTextIngredients.style.display = 'none'
        productSelectedInputIngredients.style.display = 'block'
    })


    const productSelectedInputIngredients = html('input', {
        display: (oldDataProductSelected.ingredients != product.ingredients) ? ('block') : ('none'),
        cursor: 'default',
    }, {
        placeholder: (oldDataProductSelected.ingredients != product.ingredients) ? (product.ingredients) : ("NUEVOS INGREDIENTES"),
        id: 'productSelectedInputIngredients',
        class: 'productSelectedElement productSelectedInput',
        name : 'ingredients'
    },
        dataDivContainer)


    const productSelectedCancelateIndividualPropietyButtonIngredients = html('button', {
        opacity: (oldDataProductSelected.ingredients != product.ingredients) ? (1) : (0),
    }, {
        textContent: 'DESCARTAR CAMBIO',
        id: 'productSelectedCancelateIndividualPropietyButtonIngredients',
        class: 'productSelectedElement cancelateIndividualPropietyButton'
    },
        dataDivContainer)

    productSelectedCancelateIndividualPropietyButtonIngredients.addEventListener('click', () => {
        productSelectedCancelateIndividualPropietyButtonIngredients.style.opacity = 0;
        productSelectedTextIngredients.style.display = 'block'
        productSelectedInputIngredients.style.display = 'none'
        productSelectedInputIngredients.value = ''
        discardAChange(product._id, 'ingredients')
    })

    // PRODUCT PRICE

    const productSelectedTextPrice = html('h3', {
        display: (oldDataProductSelected.price != product.price) ? ('none') : ('block'),
    }, {
        textContent: "Precio: $" + oldDataProductSelected.price,
        id: 'productSelectedTextPrice',
        class: 'productSelectedElement productSelectedText'
    },
        dataDivContainer)


    productSelectedTextPrice.addEventListener('click', () => {
        productSelectedCancelateIndividualPropietyButtonPrice.style.opacity = 1;
        productSelectedTextPrice.style.display = 'none'
        productSelectedInputPrice.style.display = 'block'
    })

    const productSelectedInputPrice = html('input', {
        display: (oldDataProductSelected.price != product.price) ? ('block') : ('none'),
        cursor: 'default',
    }, {
        placeholder:(oldDataProductSelected.price != product.price) ? (product.price) : ("NUEVO PRECIO"),
        type: "number",
        id: 'productSelectedInputPrice',
        class: 'productSelectedElement productSelectedInput',
        name : 'price'
    },
        dataDivContainer)


    const productSelectedCancelateIndividualPropietyButtonPrice = html('button', {
        opacity: (oldDataProductSelected.price != product.price) ? (1) : (0),
    }, {
        textContent: 'DESCARTAR CAMBIO',
        id: 'productSelectedCancelateIndividualPropietyButtonIngredients',
        class: 'productSelectedElement  cancelateIndividualPropietyButton'
    },
        dataDivContainer)

    productSelectedCancelateIndividualPropietyButtonPrice.addEventListener('click', () => {
        productSelectedCancelateIndividualPropietyButtonPrice.style.opacity = 0;
        productSelectedTextPrice.style.display = 'block'
        productSelectedInputPrice.style.display = 'none'
        productSelectedInputPrice.value = ''
        discardAChange(product._id, 'price')
    })

    // PRODUCT IMAGE
    const productSelectedImage = html('img', {
    }, {
        src: product.imgUrl,
        id: 'productSelectedImage',
        class: 'productSelectedElement productSelectedImage'
    },
        dataDivContainer)


    productSelectedImage.addEventListener('click', () => {
        productSelectedCPreviewButtonImage.style.opacity = 1;
        productSelectedCancelateIndividualPropietyButtonImage.style.opacity = 1;
        productSelectedInputImage.style.display = 'block'
    })


    const productSelectedInputImage = html('input', {
        display: (oldDataProductSelected.imgUrl != product.imgUrl) ? ('block') : ('none'),
        cursor: 'default',
    }, {
        placeholder: "NUEVA IMAGEN",
        id: 'productSelectedInputImage',
        class: 'productSelectedElement productSelectedInput',
        name : 'imgUrl'
    },
        dataDivContainer)


    const productSelectedCPreviewButtonImage = html('button', {
        opacity: (oldDataProductSelected.price != product.price) ? (1) : (0),
    }, {
        textContent: 'VISTA PREVIA',
        id: 'productSelectedCancelateIndividualPropietyButtonIngredients',
        class: 'productSelectedElement  cancelateIndividualPropietyButton productSelectedCPreviewButtonImage'
    },
        dataDivContainer)

    const productSelectedCancelateIndividualPropietyButtonImage = html('button', {
        opacity: (oldDataProductSelected.price != product.price) ? (1) : (0),
    }, {
        textContent: 'DESCARTAR CAMBIO',
        id: 'productSelectedCancelateIndividualPropietyButtonIngredients',
        class: 'productSelectedElement  cancelateIndividualPropietyButton'
    },
        dataDivContainer)

    productSelectedCancelateIndividualPropietyButtonImage.addEventListener('click', () => {
        productSelectedCPreviewButtonImage.style.opacity = 0;
        productSelectedCancelateIndividualPropietyButtonImage.style.opacity = 0;
        productSelectedInputImage.style.display = 'none'
        productSelectedInputImage.value = ''
        productSelectedImage.src = oldDataProductSelected.imgUrl;
        discardAChange(product._id, 'imgUrl')
    })

    // PRODUCT BUTTONS

    const productSelectedConfirmButton = html('button', {
        margin: "7vh 0vh 0vh 0vh",
    }, {
        textContent: 'ENVIAR CAMBIOS',
        id: 'productSelectedConfirmButton',
        class: 'productSelectedElement  productSelectedButtons'
    },
        dataDivContainer)


    const productSelectedCancelateButton = html('button', {
    }, {
        textContent: 'DESCARTAR CAMBIOS',
        id: 'productSelectedCancelateButton',
        class: 'productSelectedElement  productSelectedButtons'
    },
        dataDivContainer)

    productSelectedCancelateButton.addEventListener('click', () => {

        productSelectedCancelateIndividualPropietyButtonName.style.opacity = 0;
        productSelectedTextName.style.display = 'block'
        productSelectedInputName.value = ''
        productSelectedInputName.style.display = 'none'

        productSelectedCancelateIndividualPropietyButtonIngredients.style.opacity = 0;
        productSelectedTextIngredients.style.display = 'block'
        productSelectedInputIngredients.style.display = 'none'
        productSelectedInputIngredients.value = ''

        productSelectedCancelateIndividualPropietyButtonPrice.style.opacity = 0;
        productSelectedTextPrice.style.display = 'block'
        productSelectedInputPrice.style.display = 'none'
        productSelectedInputPrice.value = ''

        productSelectedCPreviewButtonImage.style.opacity = 0;
        productSelectedCancelateIndividualPropietyButtonImage.style.opacity = 0;
        productSelectedInputImage.style.display = 'none'
        productSelectedInputImage.value = ''

        productSelectedCPreviewButtonImage.style.opacity = 0;
        productSelectedCancelateIndividualPropietyButtonImage.style.opacity = 0;
        productSelectedInputImage.style.display = 'none'
        productSelectedInputImage.value = ''


        discardAllChangeOfAproduct(product._id)
    })




    const productSelectedDeleteButton = html('button', {
    }, {
        textContent: 'ELIMINAR PRODUCTO',
        id: 'productSelectedDeleteButton',
        class: 'productSelectedElement  productSelectedButtons'
    },
        dataDivContainer)

}

// Hidde the information about the selected Product
const hiddeInfoProductDiv = (product) => {
    changesCache(product._id)
    const selectedProductInfo = document.getElementById('dataDivContainer')
    if (selectedProductInfo != null) {
        selectedProductInfo.remove()
    }

}


//When the div is Open: Prevent the div close when is clicked (Only the selected Product's Div)
const managerDivClick = (product) => {
    productsData.map((food) => {
        if (food._id == product._id) {
            const selectedProductDivContainer = document.getElementById(`${product._id}ContainerManager`)
            if (selectedProductDivContainer.className.includes('productDesk')) {
                selectedProductDivContainer.className = 'productContainerManager';
                selectedProductDivContainer.style.cursor = 'default';
            } else {
                selectedProductDivContainer.className = 'productContainerManager productDesk';
                selectedProductDivContainer.style.cursor = 'pointer';
            }
        } else {
            document.getElementById(`${food._id}ContainerManager`).className = 'productContainerManager productDesk';
            document.getElementById(`${food._id}ContainerManager`).style.cursor = 'pointer';
        }
    })
}
