validateUserToken('../../index.html');
setCartImg('../../imgs/shopCart.png')

let currentUIMyAccount = '';
//VARIABLE TO SAVE IMAGE
let auxImg = '';
//VARIABLE TO SAVE ACTION ON THE IMAGE TESTER (SAVE / ONLY TEST)
let auxActionImage = '';
//ASK TO USER IF WANT TO CLOSE OR NOT THE ACTUAL POPUP UI
const preventUserLoseInfo = (UIfirstLayer) => {
    UIfirstLayer.style.backgroundColor = 'transparent';
    UIfirstLayer.style.opacity = 1;
    disablePopUp()

}

const testImageMyAccount = (img) => {
    const tester = new Image();
    auxImg = img;
    tester.onload = imageFoundMyAccount;
    tester.onerror = imageNotFoundMyAccount;
    tester.src = auxImg;

}

const imageFoundMyAccount = () => {
    if (auxActionImage == 'test') {
        document.getElementById('userImgMyAccount').src = auxImg;
    } else if (auxActionImage == 'save') {
        changeImage(auxImg)
    }
}

const imageNotFoundMyAccount = () => {
    showPopUpMsj('Lo siento.No se pudo encontrar la imagen.'.toUpperCase(), '-267vh 0vh 0vh 70vh', html('div', {}, { id: 'popUp' }))
    document.getElementById('userImgInpuntMyAccount').value = '';
}




const createUserCard = (userData) => {


    console.log(userData);
    const UIfirstLayer = html('div', {
        transition: 'background-color 0.3s,opacity 0.3s'
    }, {
        id: 'UIfirstLayer',
    })


    const userImgDivMyAccount = html('div', {
        transition: 'background-color 0.3s,opacity 0.3s'
    }, {
        id: 'userImgDivMyAccount',
    }, UIfirstLayer);

    const userImgMyAccount = html('img', {}, {
        id: 'userImgMyAccount',
        src: img,
    }, userImgDivMyAccount)

    userImgMyAccount.clicked = false;


    userImgMyAccount.addEventListener('click', () => {

        if (userImgMyAccount.clicked == false) {
            const actualImage = userImgMyAccount.src;


            userImgMyAccount.clicked = true;

            userImgMyAccount.style.margin = '30vh 0vh 10vh 30vh';


            const userImgInpuntMyAccount = html('input', {
                margin: '45vh 0vh 0vh 23vh'
            }, {
                id: 'userImgInpuntMyAccount',
                class: 'imageInput'
            }, userImgDivMyAccount)

            const userImgPreviewButtonMyAccount = html('button', {
                margin: '60vh 0vh 0vh 10vh',
                backgroundColor: '#285e2d',
                color: '#45b24e',
            }, {
                id :'userImgPreviewButtonMyAccount',
                class: 'imageButton',
                textContent: 'VISTA PREVIA',
            }, userImgDivMyAccount)



            userImgPreviewButtonMyAccount.addEventListener('mouseenter', () => {
                userImgPreviewButtonMyAccount.style.backgroundColor = '#45b24e';
                userImgPreviewButtonMyAccount.style.color = '#285e2d';
            })


            userImgPreviewButtonMyAccount.addEventListener('mouseleave', () => {
                userImgPreviewButtonMyAccount.style.backgroundColor = '#285e2d';
                userImgPreviewButtonMyAccount.style.color = '#45b24e';
            })

            userImgPreviewButtonMyAccount.addEventListener('click', () => {
                
                auxActionImage = 'test';
                testImageMyAccount(userImgInpuntMyAccount.value)
            })



            const userImgSaveButtonMyAccount = html('button', {
                margin: '60vh 0vh 0vh 40vh',
                backgroundColor: '#285e2d',
                color: '#45b24e',
            }, {
                id: 'userImgSaveButtonMyAccount',
                class: 'imageButton',
                textContent: 'GUARDAR CAMBIO',
            }, userImgDivMyAccount)



            userImgSaveButtonMyAccount.addEventListener('mouseenter', () => {
                userImgSaveButtonMyAccount.style.backgroundColor = '#45b24e';
                userImgSaveButtonMyAccount.style.color = '#285e2d';
            })


            userImgSaveButtonMyAccount.addEventListener('mouseleave', () => {
                userImgSaveButtonMyAccount.style.backgroundColor = '#285e2d';
                userImgSaveButtonMyAccount.style.color = '#45b24e';
            })

            userImgSaveButtonMyAccount.addEventListener('click', () => {
                auxActionImage = 'save';
                testImageMyAccount(userImgInpuntMyAccount.value)

            })




            const userImgCancelButtonMyAccount = html('button', {
                margin: '60vh 0vh 0vh 75vh',
                backgroundColor: '#742929',
                color: '#c43b3b',
            }, {
                id: 'userImgCancelButtonMyAccount',
                class: 'imageButton',
                textContent: 'CANCELAR',
            }, userImgDivMyAccount)

            userImgCancelButtonMyAccount.addEventListener('mouseenter', () => {
                userImgCancelButtonMyAccount.style.backgroundColor = '#c43b3b';
                userImgCancelButtonMyAccount.style.color = '#742929';
            })


            userImgCancelButtonMyAccount.addEventListener('mouseleave', () => {
                userImgCancelButtonMyAccount.style.backgroundColor = '#742929';
                userImgCancelButtonMyAccount.style.color = '#c43b3b';
            })

            userImgCancelButtonMyAccount.addEventListener('click', () => {
                userImgInpuntMyAccount.remove()
                userImgPreviewButtonMyAccount.remove()
                userImgCancelButtonMyAccount.remove()
                userImgSaveButtonMyAccount.remove()
                userImgMyAccount.style.margin = ' 30vh 0vh 10vh 80vh';
                userImgMyAccount.clicked = false;
                userImgMyAccount.src = actualImage;


            })
        }

    })


    const usernameDivMyAccount = html('div', {
    }, {
        id: 'usernameDivMyAccount',
    }, UIfirstLayer)


    html('h3', {
        margin: "5vh 0vh 0vh 0vh",
        cursor: 'default'

    }, {
        textContent: `NOMBRE DE USUARIO: ${userData.username}`,
        class: 'textElementMyAccount',
        id: 'usernameH3MyAccount',
    }, usernameDivMyAccount)


    usernameDivMyAccount.addEventListener('mouseenter', () => {
        onHoverDiv('usernameDivMyAccount')
    })


    usernameDivMyAccount.addEventListener('mouseleave', () => {
        leaveHoverDiv('usernameDivMyAccount')
    })


    const nameDivMyAccount = html('div', {
    }, {
        id: 'nameDivMyAccount',
    }, UIfirstLayer)



    html('h3', {
    }, {
        textContent: `NOMBRE y APELLIDO: ${userData.name}`,
        class: 'textElementMyAccount',
        id: 'nameH3MyAccount',

    },
        nameDivMyAccount)


    html('input', {
    }, {
        placeholder: userData.name,
        class: 'inputElementMyAccount',
        name: 'name',
        id: 'nameInputMyAccount',

    },
        nameDivMyAccount)



    const nameButtonMyAccount = html('button', {
        margin: "2vh 0vh 0vh 60vh",

    }, {
        textContent: `MODIFICAR NOMBRE / APELLIDO`,
        class: 'inputButtonsElementMyAccount',
        id: 'nameButtonMyAccount',
    }, nameDivMyAccount);



    nameButtonMyAccount.clicked = false


    nameDivMyAccount.addEventListener('mouseenter', () => {
        onHoverDiv('nameDivMyAccount')
    })


    nameDivMyAccount.addEventListener('mouseleave', () => {
        leaveHoverDiv('nameDivMyAccount')
    })

    nameButtonMyAccount.addEventListener('click', () => {
        if (nameButtonMyAccount.clicked == false) {
            showInputsModifyUser('name')
        } else {
            hiddeInputsModifyUser('name', 'MODIFICAR NOMBRE / APELLIDO')
            nameButtonMyAccount.style.margin = '2vh 0vh 0vh 60vh';

        }
    })


    const emailDivMyAccount = html('div', {
    }, {
        id: 'emailDivMyAccount',
    }, UIfirstLayer)


    html('h3', {
    }, {
        textContent: `CORREO ELECTRÓNICO: ${userData.email}`,
        class: 'textElementMyAccount',
        id: 'emailH3MyAccount',
    },
        emailDivMyAccount)


    html('input', {
    }, {
        placeholder: userData.email,
        class: 'inputElementMyAccount',
        name: 'email',
        id: 'emailInputMyAccount',

    },
        emailDivMyAccount)


    const emailButtonMyAccount = html('button', {
        margin: "2vh 0vh 0vh 60vh",

    }, {
        textContent: `MODIFICAR CORREO ELECTRÓNICO`,
        class: 'inputButtonsElementMyAccount',
        id: 'emailButtonMyAccount',
    }
        , emailDivMyAccount)

    emailButtonMyAccount.clicked = false



    emailDivMyAccount.addEventListener('mouseenter', () => {
        onHoverDiv('emailDivMyAccount')
    })


    emailDivMyAccount.addEventListener('mouseleave', () => {
        leaveHoverDiv('emailDivMyAccount')
    })


    emailButtonMyAccount.addEventListener('click', () => {
        if (emailButtonMyAccount.clicked == false) {
            showInputsModifyUser('email')
        } else {
            hiddeInputsModifyUser('email', 'MODIFICAR CORREO ELECTRÓNICO')
            emailButtonMyAccount.style.margin = '2vh 0vh 0vh 60vh';

        }
    })


    const phoneDivMyAccount = html('div', {
    }, {
        id: 'phoneDivMyAccount',
    }, UIfirstLayer)

    html('h3', {
    }, {
        textContent: `TELÉFONO: ${userData.phone}`,
        class: 'textElementMyAccount',
        id: 'phoneH3MyAccount',
    },
        phoneDivMyAccount)


    html('input', {
    }, {
        type: 'number',
        placeholder: userData.phone,
        class: 'inputElementMyAccount',
        name: 'phone',
        id: 'phoneInputMyAccount',

    },
        phoneDivMyAccount)


    const phoneButtonMyAccount = html('button', {
        margin: "2vh 0vh 0vh 70vh",

    }, {
        textContent: `MODIFICAR TELÉFONO`,
        class: 'inputButtonsElementMyAccount',
        id: 'phoneButtonMyAccount',
    },
        phoneDivMyAccount)


    phoneButtonMyAccount.clicked = false



    phoneDivMyAccount.addEventListener('mouseenter', () => {
        onHoverDiv('phoneDivMyAccount')
    })


    phoneDivMyAccount.addEventListener('mouseleave', () => {
        leaveHoverDiv('phoneDivMyAccount')
    })



    phoneButtonMyAccount.addEventListener('click', () => {
        if (phoneButtonMyAccount.clicked == false) {
            showInputsModifyUser('phone')
        } else {
            hiddeInputsModifyUser('phone', 'MODIFICAR TELÉFONO')
            phoneButtonMyAccount.style.margin = '2vh 0vh 0vh 70vh';
        }
    })


    html('h3', {
        cursor: 'default'
    }, {
        textContent: `TIPO DE CUENTA    : ${nameTypeAccount(userData.typesAaccount)}`,
        class: 'textElementMyAccount',
    }, UIfirstLayer)



    const confirmButtonMyAccount = html('button', {
        margin: "10vh 0vh 0vh 70vh",

    }, {
        textContent: `CONFIRMAR CAMBIOS`,
        class: 'buttonElementMyAccount',
        id: 'confirmButtonMyAccount'
    }, UIfirstLayer)

    confirmButtonMyAccount.addEventListener('click', (e) => {
        const dataValidated = validateUserData(userData);
        if (dataValidated.boolean == true) {
            updateUserData(dataValidated.data, '-100vh 0vh 0vh 50vh', html('div', {}, { id: 'popUp' }))
        } else {
            showPopUpMsj('No hay cambios que realizar.'.toUpperCase(), '-100vh 0vh 0vh 50vh', html('div', {}, { id: 'popUp' }))
        }
    })

    const changePasswordButtonMyAccount = html('button', {
        margin: "10vh 0vh 0vh 68vh",

    }, {
        textContent: `CAMBIAR CONTRASEÑA`,
        class: 'buttonElementMyAccount',
        id: 'changePasswordButtonMyAccount'
    }, UIfirstLayer)

    changePasswordButtonMyAccount.addEventListener('click', () => {
        showPopUpChangePassword(html('div', {}, { id: 'popUp' }))
    })

    const addressManagerButtonMyAccount = html('button', {
        margin: "10vh 0vh 0vh 50vh",

    }, {
        textContent: `GESTIONAR AGENDA DE DIRECCIONES`,
        class: 'buttonElementMyAccount',
        id: 'addressManagerButtonMyAccount'
    }, UIfirstLayer)

    addressManagerButtonMyAccount.addEventListener('click', () => {
        currentUIMyAccount = 'address';
        showPopUpAddressBook()

    })

    const deleteAccountButtonMyAccount = html('button', {
        margin: "10vh 0vh 20vh 75vh",

    }, {
        textContent: `BORRAR CUENTA`,
        class: 'buttonElementMyAccount',
        id: 'deleteAccountButtonMyAccount'
    }, UIfirstLayer)

    deleteAccountButtonMyAccount.addEventListener('click', () => {
        showPopUpConfirmationDeleteAccount(html('div', {}, { id: 'popUp' }))
    })




    UIfirstLayer.addEventListener('click', (e) => {
        if (
            e.target.id == 'confirmButtonMyAccount' ||
            e.target.id == 'changePasswordButtonMyAccount' ||
            e.target.id == 'addressManagerButtonMyAccount' ||
            e.target.id == 'deleteAccountButtonMyAccount'
        ) {
            UIfirstLayer.style.backgroundColor = '#000000';
            UIfirstLayer.style.opacity = 0.3;
        } else {
            if (currentUIMyAccount != '') {
                preventUserLoseInfo(UIfirstLayer)
            } else {
                UIfirstLayer.style.backgroundColor = 'transparent';
                UIfirstLayer.style.opacity = 1;
                disablePopUp()
            }
        }
    })


}


showDataUser()





