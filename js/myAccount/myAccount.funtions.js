let oldData = {};
let userData = {};
let addressArrAux;

const getUserData = async () => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    await fetch(`${backendURL}/user/showDataUser`, requestOptions)
        .then(res => res.json())
        .then(response => resp = response)
        .catch(error => console.log('error', error));
    return resp;
}


window.addEventListener("beforeunload", function (e) {
    if ((JSON.stringify(oldData) != JSON.stringify(userData))) {
        const confirmationMessage = 'It looks like you have been editing something. '
            + 'If you leave before saving, your changes will be lost.';

        (e || window.event).returnValue = confirmationMessage; //Gecko + IE
        return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
    }

});




const updateAddress = async (updatedAddress, index, htmlElementsId, popUp2) => {

    const address = {
        address: updatedAddress,
        oldAdr: index + 1,
    };

    document.body.style.cursor = 'wait';
    const resolve = await fetchUpdateAddress(address);
    if (resolve.status == 200) {
        document.body.style.cursor = 'default';
        showH3AddressBookModifyUser(
            htmlElementsId.popUpInputElementAddress,
            htmlElementsId.popUpButtonSaveElementAddress,
            htmlElementsId.popUpButtonCancelElementAddress,
            htmlElementsId.popUpH3ElementAddress,
            htmlElementsId.popUpButtonModifyElementAddress,
            htmlElementsId.popUpButtonDeleteElementAddress,
        )

        htmlElementsId.popUpH3ElementAddress.textContent = address.address;
        htmlElementsId.popUpInputElementAddress.placeholder = address.address;
        userData.address[index] = address.address;
        oldData.address[index] = address.address;

        showPopUpMsj(resolve.msj.toUpperCase(), '10vh 0vh 0vh 30vh', popUp2)



    } else {
        document.body.style.cursor = 'default';
        showH3AddressBookModifyUser(
            htmlElementsId.popUpInputElementAddress,
            htmlElementsId.popUpButtonSaveElementAddress,
            htmlElementsId.popUpButtonCancelElementAddress,
            htmlElementsId.popUpH3ElementAddress,
            htmlElementsId.popUpButtonModifyElementAddress,
            htmlElementsId.popUpButtonDeleteElementAddress,
        )

        showPopUpMsj(resolve.msj.toUpperCase(), '10vh 0vh 0vh 30vh', popUp2)
    }


}

const fetchUpdateAddress = async (address) => {

    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

    let requestOptions = {
        method: 'PUT',
        headers: myHeaders,
        redirect: 'follow',
        body: JSON.stringify(address),
    };

    await fetch(`${backendURL}/user/modAddress`, requestOptions)
        .then(res => res.json())
        .then(response => resp = response)
        .catch(error => console.log('error', error));
    return resp;
}



const sendNewAddress = async (newAddress) => {
    document.getElementById('newAddressConfirmationPopUpContainer').style.cursor = 'wait';
    const resolve = await fetchCreateAddress(newAddress);
    if (resolve.status == 200) {
        document.getElementById('newAddressConfirmationPopUpContainer').style.cursor = 'default';
        document.getElementById('newAddressConfirmationPopUpTitle').textContent = resolve.msj.toUpperCase();
        document.getElementById('newAddressConfirmationPopUpSaveButton').textContent = 'ACEPTAR';
        document.getElementById('newAddressConfirmationPopUpSaveButton').addEventListener('click', () => {
            document.getElementById('newAddressConfirmationPopUpContainer').remove()
        })
        document.getElementById('newAddressConfirmationPopUpInput').remove()
        document.getElementById('newAddressConfirmationPopUpCancelButton').remove()
        userData.address.push(newAddress)
        oldData.address.push(newAddress)

        showAddressMyAccount(document.getElementById('popUp'))
    } else {
        document.getElementById('newAddressConfirmationPopUpContainer').style.cursor = 'default';
        document.getElementById('newAddressConfirmationPopUpTitle').textContent = resolve.msj.toUpperCase();
        document.getElementById('newAddressConfirmationPopUpSaveButton').textContent = 'ACEPTAR';
        document.getElementById('newAddressConfirmationPopUpSaveButton').addEventListener('click', () => {
            document.getElementById('newAddressConfirmationPopUpContainer').remove()
        })
        document.getElementById('newAddressConfirmationPopUpInput').remove()
        document.getElementById('newAddressConfirmationPopUpCancelButton').remove()
    }
}

const fetchCreateAddress = async (newAddress) => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", window.sessionStorage.getItem('token'));


    let requestOptions = {
        method: 'POST',
        headers: myHeaders,
        redirect: 'follow',
        body: JSON.stringify({ address: newAddress }),
    };

    await fetch(`${backendURL}/user/newAddress`, requestOptions)
        .then(res => res.json())
        .then(response => resp = response)
        .catch(error => console.log('error', error));
    return resp;
}





const deleteAddress = async (index, popUp2) => {
    const address = {
        oldAdr: index + 1,
    }

    document.body.style.cursor = 'wait';
    const resolve = await fetchDeleteAddress(address);
    if (resolve.status == 200) {
        document.body.style.cursor = 'default';
        document.getElementById('confirmationPopUpContainer').remove()
        showPopUpMsj(resolve.msj.toUpperCase(), '10vh 0vh 0vh 30vh', popUp2)
        const addressToDelete = userData.address[index];
        userData.address = userData.address.filter(userData => userData != addressToDelete);
        oldData.address = oldData.address.filter(oldData => oldData != addressToDelete);
        showAddressMyAccount(popUp2)
    } else {
        document.body.style.cursor = 'default';
        document.getElementById('confirmationPopUpContainer').remove()
        showPopUpMsj(resolve.msj.toUpperCase(), '10vh 0vh 0vh 30vh', popUp2)

    }

}


const fetchDeleteAddress = async (address) => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

    let requestOptions = {
        method: 'DELETE',
        headers: myHeaders,
        redirect: 'follow',
        body: JSON.stringify(address),
    };

    await fetch(`${backendURL}/user/delAddress`, requestOptions)
        .then(res => res.json())
        .then(response => resp = response)
        .catch(error => console.log('error', error));
    return resp;
}

const validateUserData = (userData) => {
    const modData = {
        name: userData.name.trim(),
        phone: (isNaN(userData.phone) ? (false) : (userData.phone)),
        email: userData.email.trim()
    };

    const finalData = {
        boolean: false,
        data: {}
    };



    (modData.name.length == 0 || oldData.name == modData.name)
        ? (
            finalData.boolean = false || finalData.boolean
        )
        : (
            finalData.boolean = true || finalData.boolean,
            finalData.data.name = modData.name
        );




    (modData.email.length == 0 || oldData.email == modData.email)
        ? (
            finalData.boolean = false || finalData.boolean
        )
        : (
            finalData.boolean = true || finalData.boolean,
            finalData.data.email = modData.email
        );



    (modData.phone == false || oldData.phone == modData.phone)
        ? (
            finalData.boolean = false || finalData.boolean
        )
        : (
            finalData.boolean = true || finalData.boolean,
            finalData.data.phone = modData.phone
        );

    return finalData

}



const updateUserData = async (newDataUser, margin, popUp2) => {
    document.body.style.cursor = 'wait';
    const resolve = await updateUser(newDataUser);
    if (resolve.status == 200) {
        alert('DATOS GUARDADOS')
        oldData = userData;
        window.location.reload();
    } else {
        showPopUpMsj(resolve.msj.toUpperCase(), margin, popUp2)
    }
}

const updateUser = async (dataUser) => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

    let requestOptions = {
        method: 'PUT',
        headers: myHeaders,
        redirect: 'follow',
        body: JSON.stringify(dataUser),
    };

    await fetch(`${backendURL}/user/modUser`, requestOptions)
        .then(res => res.json())
        .then(response => resp = response)
        .catch(error => console.log('error', error));
    return resp;
}


const changePassword = async (oldPassword, newPassword) => {
    document.body.style.cursor = 'wait';
    const resolve = await fetchChangePassword(oldPassword, newPassword);
    if (resolve.status == 200) {
        alert('CONTRASEÑA CAMBIADA! REDIRIGIENDO A LOGIN')
        sessionStorage.clear();
        window.location.reload();

    } else {
        showPopUpMsj(resolve.msj.toUpperCase(), '-75vh 0vh 0vh 80vh', html('div', {}, { id: 'popUp' }))
        document.body.style.cursor = 'default';
    }
}


const fetchChangePassword = async (oldPassword, newPassword) => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", window.sessionStorage.getItem('token'));


    let requestOptions = {
        method: 'POST',
        headers: myHeaders,
        redirect: 'follow',
        body: JSON.stringify({ oldPassword: oldPassword, newPassword: newPassword }),
    };

    await fetch(`${backendURL}/user/changePassword`, requestOptions)
        .then(res => res.json())
        .then(response => resp = response)
        .catch(error => console.log('error', error));
    return resp;
}

const changeImage = async (newImage) => {
    document.body.style.cursor = 'wait';
    const resolve = await fetchChangeImage(newImage);
    if (resolve.status == 200) {
        document.body.style.cursor = 'default';
        showPopUpMsj(resolve.msj.toUpperCase(), '-267vh 0vh 0vh 70vh', html('div', {}, { id: 'popUp' }))
        document.getElementById('userImgMyAccount').src = newImage;
        document.getElementById('userImage').src = newImage;
        window.sessionStorage.setItem('userImg',JSON.stringify(newImage))

        document.getElementById('userImgInpuntMyAccount').remove()
        document.getElementById('userImgPreviewButtonMyAccount').remove()
        document.getElementById('userImgSaveButtonMyAccount').remove()
        document.getElementById('userImgCancelButtonMyAccount').remove()
        document.getElementById('userImgMyAccount').style.margin = ' 30vh 0vh 10vh 80vh';
        document.getElementById('userImgMyAccount').clicked = false;
        document.getElementById('userImgMyAccount').src = newImage;


        auxImg = ''
    } else {
        showPopUpMsj(resolve.msj.toUpperCase(), '-267vh 0vh 0vh 70vh', html('div', {}, { id: 'popUp' }))
        document.body.style.cursor = 'default';
    }
}


const fetchChangeImage = async (newImage) => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", window.sessionStorage.getItem('token'));


    let requestOptions = {
        method: 'PUT',
        headers: myHeaders,
        redirect: 'follow',
        body: JSON.stringify({ userImg: newImage }),
    };

    await fetch(`${backendURL}/user/changeUserImg`, requestOptions)
        .then(res => res.json())
        .then(response => resp = response)
        .catch(error => console.log('error', error));
    return resp;
}


const deleteMyAccount = async () => {
    document.body.style.cursor = 'wait';
    const resolve = await fetchDeleteAccount();
    if (resolve.status == 200) {
        alert('TE EXTRAÑAREMOS =(')
        sessionStorage.clear();
        window.location.reload();

    } else {
        showPopUpMsj(resolve.msj.toUpperCase(), '-75vh 0vh 0vh 80vh', html('div', {}, { id: 'popUp' }))
        document.body.style.cursor = 'default';
    }
}


const fetchDeleteAccount = async () => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", window.sessionStorage.getItem('token'));


    let requestOptions = {
        method: 'DELETE',
        headers: myHeaders,
        redirect: 'follow',
    };

    await fetch(`${backendURL}/user/deleteUser`, requestOptions)
        .then(res => res.json())
        .then(response => resp = response)
        .catch(error => console.log('error', error));
    return resp;
}
