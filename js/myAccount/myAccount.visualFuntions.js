

const showDataUser = async () => {
    userData = await getUserData()
    oldData = structuredClone(userData)
    createUserCard(userData)
    addressArrAux = userData.address.map((address, index) => {
        return { address: address, id: index }
    });



}


const nameTypeAccount = (typeAaccount) => {
    switch (typeAaccount) {
        case 0: return 'Local';
        case 1: return 'Google';
        case 2: return 'Linkedin';
        case 3: return 'Facebook';
    }
}


const onHoverDiv = (divID) => {
    const divElements = document.getElementById(divID)
    const divNodes = divElements.childNodes

    for (let i = 0; i < divNodes.length; i++) {
        if (divNodes[i].localName == 'button') {
            divNodes[i].style.opacity = 1
        } else {
            divNodes[i].style.fontSize = "8vh"
            divNodes[i].style.color = '#8284e0'
        }
    }
}

const leaveHoverDiv = (divID) => {
    const divElements = document.getElementById(divID)
    const divNodes = divElements.childNodes

    for (let i = 0; i < divNodes.length; i++) {
        if (divNodes[i].localName == 'button') {
            divNodes[i].style.opacity = 0
        } else {
            divNodes[i].style.fontSize = "5vh"
            divNodes[i].style.color = '#414394'
        }
    }
}


const showInputsModifyUser = (propiety) => {
    const input = document.getElementById(`${propiety}InputMyAccount`);
    const h3 = document.getElementById(`${propiety}H3MyAccount`);
    const button = document.getElementById(`${propiety}ButtonMyAccount`);

    input.style.display = 'block';
    h3.style.display = 'none';
    button.clicked = true;
    button.style.backgroundColor = "#cc5050";
    button.style.color = "#c72727";
    button.textContent = 'CANCELAR CAMBIO';
    button.style.margin = "2vh 0vh 0vh 80vh";
    input.onchange = (e) => {
        userData[propiety] = e.target.value;
    }
}


const hiddeInputsModifyUser = (propiety, originalText) => {
    const input = document.getElementById(`${propiety}InputMyAccount`);
    const h3 = document.getElementById(`${propiety}H3MyAccount`);
    const button = document.getElementById(`${propiety}ButtonMyAccount`);

    input.value = '';
    input.style.display = 'none';
    h3.style.display = 'block';
    button.clicked = false;
    button.style.backgroundColor = "#429e44";
    button.style.color = "#75b576";
    button.textContent = originalText;
    userData[propiety] = oldData[propiety];
    
}




const disablePopUp = () => {
    try {
        document.getElementById('popUp').remove()
        document.body.style.overflow = "";
    } catch (error) {

    }
}


