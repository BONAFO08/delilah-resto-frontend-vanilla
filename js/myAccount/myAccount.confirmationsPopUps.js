
const createNewAddressPopUp = (popUp2) => {

    const newAddressConfirmationPopUpContainer = html('div', {

        margin: '10vh 0vh 0vh 30vh',

    }, {
        class: 'containerConfirmationPopUp',
        id: 'newAddressConfirmationPopUpContainer'
    }, popUp2);


    const newAddressConfirmationPopUpTitle = html('h3', {
    }, {
        id: 'newAddressConfirmationPopUpTitle',
        class: 'titleConfirmationPopUp',
        textContent: 'AGREGAR NUEVO DOMICILIO',

    }, newAddressConfirmationPopUpContainer);



    const newAddressConfirmationPopUpInput = html('input', {
        margin: '5vh 0vh 0vh 3vh ',
    }, {
        id: 'newAddressConfirmationPopUpInput',
        class: 'inputConfirmationPopUp',
        placeholder: 'NUEVO DOMICILIO',
    }, newAddressConfirmationPopUpContainer);


    html('br', {}, {}, newAddressConfirmationPopUpContainer)

    const newAddressConfirmationPopUpSaveButton = html('button', {
        color: "#45b24e",
        backgroundColor: '#285e2d',
        margin: '5vh 0vh 0vh 3vh ',
    }, {
        id: 'newAddressConfirmationPopUpSaveButton',
        class: 'actionButtonsConfirmationPopUp',
        textContent: 'AGREGAR',
    }, newAddressConfirmationPopUpContainer);


    newAddressConfirmationPopUpSaveButton.addEventListener('mouseenter', () => {
        newAddressConfirmationPopUpSaveButton.style.backgroundColor = '#45b24e';
        newAddressConfirmationPopUpSaveButton.style.color = '#285e2d';
    })


    newAddressConfirmationPopUpSaveButton.addEventListener('mouseleave', () => {
        newAddressConfirmationPopUpSaveButton.style.backgroundColor = '#285e2d';
        newAddressConfirmationPopUpSaveButton.style.color = '#45b24e';
    })

    newAddressConfirmationPopUpSaveButton.addEventListener('click', () => {
        const newAddress = validateAddressAfterSavedMyAccount(newAddressConfirmationPopUpInput.value);

        if (newAddress.boolean == true) {
            sendNewAddress(newAddress.address)
        } else {
            newAddressConfirmationPopUpTitle.textContent = newAddress.msj.toUpperCase();
            newAddressConfirmationPopUpSaveButton.textContent = 'ACEPTAR';
            newAddressConfirmationPopUpSaveButton.addEventListener('click', () => {
                newAddressConfirmationPopUpContainer.remove()
            })
            newAddressConfirmationPopUpInput.remove()
            newAddressConfirmationPopUpCancelButton.remove()
        }

    })


    const newAddressConfirmationPopUpCancelButton = html('button', {
        color: "#c43b3b",
        backgroundColor: '#742929',
        margin: '5vh 0vh 0vh 3vh ',
    }, {
        id: 'newAddressConfirmationPopUpCancelButton',
        class: 'actionButtonsConfirmationPopUp',
        textContent: 'CANCELAR',
    }, newAddressConfirmationPopUpContainer);

    newAddressConfirmationPopUpCancelButton.addEventListener('mouseenter', () => {
        newAddressConfirmationPopUpCancelButton.style.backgroundColor = '#c43b3b';
        newAddressConfirmationPopUpCancelButton.style.color = '#742929';
    })


    newAddressConfirmationPopUpCancelButton.addEventListener('mouseleave', () => {
        newAddressConfirmationPopUpCancelButton.style.backgroundColor = '#742929';
        newAddressConfirmationPopUpCancelButton.style.color = '#c43b3b';
    })

    newAddressConfirmationPopUpCancelButton.addEventListener('click', () => {
        newAddressConfirmationPopUpContainer.remove()
    })


}
