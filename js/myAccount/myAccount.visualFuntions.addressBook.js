
let currentMyAccount = 0;
const changesAddressBook = [];

const validateNewPassword = (newPassword, newPasswordRepeat, oldPassword) => {
    const passwords = {
        newPassword: newPassword.trim(),
        newPasswordRepeat: newPasswordRepeat.trim(),
        oldPassword: oldPassword.trim(),
    }

    if (passwords.newPassword.length != 0 && passwords.newPasswordRepeat.length != 0 && passwords.oldPassword.length != 0) {
        if (passwords.newPassword === passwords.newPasswordRepeat) {
            return { newPassword: passwords.newPassword, boolean: true, oldPassword: passwords.oldPassword }
        } else {
            return { msj: 'Las contraseñas no son iguales.', boolean: false }
        }
    } else {
        return { msj: 'Datos Invalidos.', boolean: false }
    }
}

const validateAddressAfterSavedMyAccount = (address) => {
    try {
        let newAddress = (address.toString()).trim();
        if (newAddress.length > 0) {
            let validator = userData.address.filter(userData => userData == newAddress);
            if (validator.length == 0) {
                return { address: newAddress, boolean: true }
            } else {
                return { msj: 'El domicilio ya esta registrado en su agenda.', boolean: false }
            }
        } else {
            return { msj: 'Datos Invalidos.', boolean: false }
        }
    } catch (error) {
        return { msj: 'Datos Invalidos.', boolean: false }
    }

}


const showPopUpConfirmationDeleteAccount = (popUp2) => {

    const confirmationPopUpContainer = html('div', {

        margin: '-50vh 0vh 0vh 40vh',

    }, {
        class: 'containerConfirmationPopUp',
        id: 'confirmationPopUpContainer'
    }, popUp2);


    const confirmationPopUpTitle = html('h3', {
    }, {
        id: 'confirmationPopUpTitle',
        class: 'titleConfirmationPopUp',
    }, confirmationPopUpContainer);
    confirmationPopUpTitle.innerText = `¿ESTA SEGURO/A DE ELIMINAR SU CUENTA? \n (ESTA ACCION NO SE PUEDE DESHACER)`,


        html('br', {}, {}, confirmationPopUpContainer)

    const confirmationPopUpSaveButton = html('button', {
        color: "#45b24e",
        backgroundColor: '#285e2d',
        margin: '5vh 0vh 0vh 3vh ',
    }, {
        id: 'confirmationPopUpSaveButton',
        class: 'actionButtonsConfirmationPopUp',
        textContent: 'ACEPTAR',
    }, confirmationPopUpContainer);


    confirmationPopUpSaveButton.addEventListener('mouseenter', () => {
        confirmationPopUpSaveButton.style.backgroundColor = '#45b24e';
        confirmationPopUpSaveButton.style.color = '#285e2d';
    })


    confirmationPopUpSaveButton.addEventListener('mouseleave', () => {
        confirmationPopUpSaveButton.style.backgroundColor = '#285e2d';
        confirmationPopUpSaveButton.style.color = '#45b24e';
    })

    confirmationPopUpSaveButton.addEventListener('click', () => {
        deleteMyAccount();
    })


    const confirmationPopUpCancelButton = html('button', {
        color: "#c43b3b",
        backgroundColor: '#742929',
        margin: '5vh 0vh 0vh 3vh ',
    }, {
        id: 'confirmationPopUpCancelButton',
        class: 'actionButtonsConfirmationPopUp',
        textContent: 'CANCELAR',
    }, confirmationPopUpContainer);

    confirmationPopUpCancelButton.addEventListener('mouseenter', () => {
        confirmationPopUpCancelButton.style.backgroundColor = '#c43b3b';
        confirmationPopUpCancelButton.style.color = '#742929';
    })


    confirmationPopUpCancelButton.addEventListener('mouseleave', () => {
        confirmationPopUpCancelButton.style.backgroundColor = '#742929';
        confirmationPopUpCancelButton.style.color = '#c43b3b';
    })

    confirmationPopUpCancelButton.addEventListener('click', () => {
        confirmationPopUpContainer.remove()
    })


}

const showPopUpConfirmationDeleteAddress = (popUp2, address, index) => {

    const confirmationPopUpContainer = html('div', {

        margin: '10vh 0vh 0vh 30vh',

    }, {
        class: 'containerConfirmationPopUp',
        id: 'confirmationPopUpContainer'
    }, popUp2);


    const confirmationPopUpTitle = html('h3', {
    }, {
        id: 'confirmationPopUpTitle',
        class: 'titleConfirmationPopUp',
    }, confirmationPopUpContainer);
    confirmationPopUpTitle.innerHTML = `¿ESTA SEGURO/A DE ELIMINAR <span style="color:#b46d3b">${address.toUpperCase()}</span> DE SU AGENDA DE  DIRECCIONES?`,


        html('br', {}, {}, confirmationPopUpContainer)

    const confirmationPopUpSaveButton = html('button', {
        color: "#45b24e",
        backgroundColor: '#285e2d',
        margin: '5vh 0vh 0vh 3vh ',
    }, {
        id: 'confirmationPopUpSaveButton',
        class: 'actionButtonsConfirmationPopUp',
        textContent: 'ACEPTAR',
    }, confirmationPopUpContainer);


    confirmationPopUpSaveButton.addEventListener('mouseenter', () => {
        confirmationPopUpSaveButton.style.backgroundColor = '#45b24e';
        confirmationPopUpSaveButton.style.color = '#285e2d';
    })


    confirmationPopUpSaveButton.addEventListener('mouseleave', () => {
        confirmationPopUpSaveButton.style.backgroundColor = '#285e2d';
        confirmationPopUpSaveButton.style.color = '#45b24e';
    })

    confirmationPopUpSaveButton.addEventListener('click', () => {
        deleteAddress(index, popUp2)


    })


    const confirmationPopUpCancelButton = html('button', {
        color: "#c43b3b",
        backgroundColor: '#742929',
        margin: '5vh 0vh 0vh 3vh ',
    }, {
        id: 'confirmationPopUpCancelButton',
        class: 'actionButtonsConfirmationPopUp',
        textContent: 'CANCELAR',
    }, confirmationPopUpContainer);

    confirmationPopUpCancelButton.addEventListener('mouseenter', () => {
        confirmationPopUpCancelButton.style.backgroundColor = '#c43b3b';
        confirmationPopUpCancelButton.style.color = '#742929';
    })


    confirmationPopUpCancelButton.addEventListener('mouseleave', () => {
        confirmationPopUpCancelButton.style.backgroundColor = '#742929';
        confirmationPopUpCancelButton.style.color = '#c43b3b';
    })

    confirmationPopUpCancelButton.addEventListener('click', () => {
        confirmationPopUpContainer.remove()
    })


}


const showPopUpChangePassword = (popUp2) => {

    const changePasswordConfirmationPopUpContainer = html('div', {

        margin: '-90vh 0vh 0vh 60vh',

    }, {
        class: 'containerConfirmationPopUp',
        id: 'changePasswordConfirmationPopUpContainer'
    }, popUp2);


    const changePasswordConfirmationPopUpTitle = html('h3', {
    }, {
        id: 'changePasswordConfirmationPopUpTitle',
        class: 'titleConfirmationPopUp',
        textContent: 'CAMBIAR CONTRASEÑA',

    }, changePasswordConfirmationPopUpContainer);



    const oldPasswordConfirmationPopUpInput = html('input', {
        margin: '5vh 0vh 0vh 3vh ',
    }, {
        id: 'changePasswordConfirmationPopUpInput',
        class: 'inputConfirmationPopUp',
        placeholder: 'VIEJA CONTRASEÑA',
        type: 'password',
    }, changePasswordConfirmationPopUpContainer);


    const showHiddeOldPasswordConfirmationPopUpInput = html('img', {
        margin: '5vh 0vh 0vh 7vh ',
        width: '10vh',
        height: '5vh',
        cursor: 'pointer'
    }, {
        id: 'changePasswordConfirmationPopUpInput',
        src: '../../imgs/show-password.png',
    }, changePasswordConfirmationPopUpContainer);


    showHiddeOldPasswordConfirmationPopUpInput.addEventListener('mouseenter', () => {
        const inputType = oldPasswordConfirmationPopUpInput.type;
        if (inputType == 'password') {
            showHiddeOldPasswordConfirmationPopUpInput.src = '../../imgs/show-password-hover.png';
        } else {
            showHiddeOldPasswordConfirmationPopUpInput.src = '../../imgs/hidde-password-hover.png';
        }
    })


    showHiddeOldPasswordConfirmationPopUpInput.addEventListener('mouseleave', () => {
        const inputType = oldPasswordConfirmationPopUpInput.type;
        if (inputType == 'password') {
            showHiddeOldPasswordConfirmationPopUpInput.src = '../../imgs/show-password.png';
        } else {
            showHiddeOldPasswordConfirmationPopUpInput.src = '../../imgs/hidde-password.png';
        }
    })

    showHiddeOldPasswordConfirmationPopUpInput.addEventListener('click', () => {
        const inputType = oldPasswordConfirmationPopUpInput.type;
        if (inputType == 'password') {
            oldPasswordConfirmationPopUpInput.type = 'text';
            showHiddeOldPasswordConfirmationPopUpInput.src = '../../imgs/hidde-password.png';
        } else {
            oldPasswordConfirmationPopUpInput.type = 'password';
            showHiddeOldPasswordConfirmationPopUpInput.src = '../../imgs/show-password.png';

        }

    })


    html('br', {}, {}, changePasswordConfirmationPopUpContainer)

    const newPasswordConfirmationPopUpInput = html('input', {
        margin: '5vh 0vh 0vh 3vh ',
    }, {
        id: 'changePasswordConfirmationPopUpInput',
        class: 'inputConfirmationPopUp',
        placeholder: 'NUEVA CONTRASEÑA',
        type: 'password',
    }, changePasswordConfirmationPopUpContainer);

    const showHiddeNewPasswordConfirmationPopUpInput = html('img', {
        margin: '5vh 0vh 0vh 7vh ',
        width: '10vh',
        height: '5vh',
        cursor: 'pointer'
    }, {
        id: 'changePasswordConfirmationPopUpInput',
        src: '../../imgs/show-password.png',
    }, changePasswordConfirmationPopUpContainer);



    showHiddeNewPasswordConfirmationPopUpInput.addEventListener('mouseenter', () => {
        const inputType = newPasswordConfirmationPopUpInput.type;
        if (inputType == 'password') {
            showHiddeNewPasswordConfirmationPopUpInput.src = '../../imgs/show-password-hover.png';
        } else {
            showHiddeNewPasswordConfirmationPopUpInput.src = '../../imgs/hidde-password-hover.png';
        }
    })


    showHiddeNewPasswordConfirmationPopUpInput.addEventListener('mouseleave', () => {
        const inputType = newPasswordConfirmationPopUpInput.type;
        if (inputType == 'password') {
            showHiddeNewPasswordConfirmationPopUpInput.src = '../../imgs/show-password.png';
        } else {
            showHiddeNewPasswordConfirmationPopUpInput.src = '../../imgs/hidde-password.png';
        }
    })

    showHiddeNewPasswordConfirmationPopUpInput.addEventListener('click', () => {
        const inputType = newPasswordConfirmationPopUpInput.type;
        if (inputType == 'password') {
            newPasswordConfirmationPopUpInput.type = 'text';
            showHiddeNewPasswordConfirmationPopUpInput.src = '../../imgs/hidde-password.png';
        } else {
            newPasswordConfirmationPopUpInput.type = 'password';
            showHiddeNewPasswordConfirmationPopUpInput.src = '../../imgs/show-password.png';

        }

    })


    html('br', {}, {}, changePasswordConfirmationPopUpContainer)

    const newPasswordRepeatConfirmationPopUpInput = html('input', {
        margin: '5vh 0vh 0vh 3vh ',
    }, {
        id: 'changePasswordConfirmationPopUpInput',
        class: 'inputConfirmationPopUp',
        placeholder: 'REPETIR NUEVA CONTRASEÑA',
        type: 'password',
    }, changePasswordConfirmationPopUpContainer);

    const showHiddeNewPasswordRepeatConfirmationPopUpInput = html('img', {
        margin: '5vh 0vh 0vh 7vh ',
        width: '10vh',
        height: '5vh',
        cursor: 'pointer'
    }, {
        id: 'changePasswordConfirmationPopUpInput',
        src: '../../imgs/show-password.png',
    }, changePasswordConfirmationPopUpContainer);

    showHiddeNewPasswordRepeatConfirmationPopUpInput.addEventListener('mouseenter', () => {
        const inputType = newPasswordRepeatConfirmationPopUpInput.type;
        if (inputType == 'password') {
            showHiddeNewPasswordRepeatConfirmationPopUpInput.src = '../../imgs/show-password-hover.png';
        } else {
            showHiddeNewPasswordRepeatConfirmationPopUpInput.src = '../../imgs/hidde-password-hover.png';
        }
    })


    showHiddeNewPasswordRepeatConfirmationPopUpInput.addEventListener('mouseleave', () => {
        const inputType = newPasswordRepeatConfirmationPopUpInput.type;
        if (inputType == 'password') {
            showHiddeNewPasswordRepeatConfirmationPopUpInput.src = '../../imgs/show-password.png';
        } else {
            showHiddeNewPasswordRepeatConfirmationPopUpInput.src = '../../imgs/hidde-password.png';
        }
    })

    showHiddeNewPasswordRepeatConfirmationPopUpInput.addEventListener('click', () => {
        const inputType = newPasswordRepeatConfirmationPopUpInput.type;
        if (inputType == 'password') {
            newPasswordRepeatConfirmationPopUpInput.type = 'text';
            showHiddeNewPasswordRepeatConfirmationPopUpInput.src = '../../imgs/hidde-password.png';
        } else {
            newPasswordRepeatConfirmationPopUpInput.type = 'password';
            showHiddeNewPasswordRepeatConfirmationPopUpInput.src = '../../imgs/show-password.png';

        }

    })


    html('br', {}, {}, changePasswordConfirmationPopUpContainer)

    const changePassowordConfirmationPopUpSaveButton = html('button', {
        color: "#45b24e",
        backgroundColor: '#285e2d',
        margin: '10vh 0vh 0vh 3vh ',
    }, {
        id: 'changePassowordConfirmationPopUpSaveButton',
        class: 'actionButtonsConfirmationPopUp',
        textContent: 'ENVIAR',
    }, changePasswordConfirmationPopUpContainer);


    changePassowordConfirmationPopUpSaveButton.addEventListener('mouseenter', () => {
        changePassowordConfirmationPopUpSaveButton.style.backgroundColor = '#45b24e';
        changePassowordConfirmationPopUpSaveButton.style.color = '#285e2d';
    })


    changePassowordConfirmationPopUpSaveButton.addEventListener('mouseleave', () => {
        changePassowordConfirmationPopUpSaveButton.style.backgroundColor = '#285e2d';
        changePassowordConfirmationPopUpSaveButton.style.color = '#45b24e';
    })

    changePassowordConfirmationPopUpSaveButton.addEventListener('click', () => {
        const oldPassword = oldPasswordConfirmationPopUpInput.value;
        const newPassword = newPasswordConfirmationPopUpInput.value;
        const newPasswordRepeat = newPasswordRepeatConfirmationPopUpInput.value;

        const passwordCleaned = validateNewPassword(newPassword, newPasswordRepeat, oldPassword);


        if (passwordCleaned.boolean) {
            changePassword(passwordCleaned.oldPassword, passwordCleaned.newPassword)
        } else {
            showPopUpMsj(passwordCleaned.msj.toUpperCase(), '-75vh 0vh 0vh 80vh', html('div', {}, { id: 'popUp' }))
        }

        oldPasswordConfirmationPopUpInput.value = '';
        newPasswordConfirmationPopUpInput.value = '';
        newPasswordRepeatConfirmationPopUpInput.value = '';

    })


    const changePasswordConfirmationPopUpCancelButton = html('button', {
        color: "#c43b3b",
        backgroundColor: '#742929',
        margin: '10vh 0vh 0vh 12vh ',
    }, {
        id: 'changePasswordConfirmationPopUpCancelButton',
        class: 'actionButtonsConfirmationPopUp',
        textContent: 'CANCELAR',
    }, changePasswordConfirmationPopUpContainer);

    changePasswordConfirmationPopUpCancelButton.addEventListener('mouseenter', () => {
        changePasswordConfirmationPopUpCancelButton.style.backgroundColor = '#c43b3b';
        changePasswordConfirmationPopUpCancelButton.style.color = '#742929';
    })


    changePasswordConfirmationPopUpCancelButton.addEventListener('mouseleave', () => {
        changePasswordConfirmationPopUpCancelButton.style.backgroundColor = '#742929';
        changePasswordConfirmationPopUpCancelButton.style.color = '#c43b3b';
    })

    changePasswordConfirmationPopUpCancelButton.addEventListener('click', () => {
        changePasswordConfirmationPopUpContainer.remove()
    })


}


const showPopUpMsj = (msj, margin, popUp2) => {

    const showMSJPopUpContainer = html('div', {

        margin: margin,

    }, {
        class: 'containerConfirmationPopUp',
        id: 'showMSJPopUpContainer'
    }, popUp2);


    const showMSJPopUpTitle = html('h3', {
    }, {
        id: 'showMSJPopUpTitle',
        class: 'titleConfirmationPopUp',
        textContent: msj,

    }, showMSJPopUpContainer);



    html('br', {}, {}, showMSJPopUpContainer)

    const showMSJPopUpSaveButton = html('button', {
        color: "#45b24e",
        backgroundColor: '#285e2d',
        margin: '5vh 0vh 0vh 3vh ',
    }, {
        id: 'showMSJPopUpSaveButton',
        class: 'actionButtonsConfirmationPopUp',
        textContent: 'ACEPTAR',
    }, showMSJPopUpContainer);


    showMSJPopUpSaveButton.addEventListener('mouseenter', () => {
        showMSJPopUpSaveButton.style.backgroundColor = '#45b24e';
        showMSJPopUpSaveButton.style.color = '#285e2d';
    })


    showMSJPopUpSaveButton.addEventListener('mouseleave', () => {
        showMSJPopUpSaveButton.style.backgroundColor = '#285e2d';
        showMSJPopUpSaveButton.style.color = '#45b24e';
    })

    showMSJPopUpSaveButton.addEventListener('click', () => {
        document.getElementById('showMSJPopUpContainer').remove()
    })
}



const showInputAddressBookModifyUser = (
    popUpInputElementAddress,
    popUpButtonSaveElementAddress,
    popUpButtonCancelElementAddress,
    popUpH3ElementAddress,
    popUpButtonModifyElementAddress,
    popUpButtonDeleteElementAddress,
) => {
    popUpInputElementAddress.style.display = 'inline';
    popUpButtonSaveElementAddress.style.display = 'inline';
    popUpButtonCancelElementAddress.style.display = 'inline';


    popUpH3ElementAddress.style.display = 'none';
    popUpButtonModifyElementAddress.style.display = 'none';
    popUpButtonDeleteElementAddress.style.display = 'none';
}


const showH3AddressBookModifyUser = (
    popUpInputElementAddress,
    popUpButtonSaveElementAddress,
    popUpButtonCancelElementAddress,
    popUpH3ElementAddress,
    popUpButtonModifyElementAddress,
    popUpButtonDeleteElementAddress,
) => {
    popUpInputElementAddress.style.display = 'none';
    popUpButtonSaveElementAddress.style.display = 'none';
    popUpButtonCancelElementAddress.style.display = 'none';


    popUpH3ElementAddress.style.display = 'inline';
    popUpButtonModifyElementAddress.style.display = 'inline';
    popUpButtonDeleteElementAddress.style.display = 'inline';
}

const showAddressMyAccount = (popUp2) => {



    try {
        document.getElementById('containerPop').remove()
    } catch (error) {
    }


    const containerPop = html('div', {
    }, {
        id: 'containerPop'
    },
        popUp2)

    for (let i = 0; i < userData.address.length; i++) {



        if (Math.floor((i / 4)) == currentMyAccount) {

            const popUpH3ElementAddress = html('h3', {
                margin: `${(4 * ((i % 4) + 1) + (13 * (i % 4)))}vh 0vh 0vh 6vh`,
            }, {
                textContent: userData.address[i],
                class: 'popUpH3ElementAddressBook',
                id: `popUpH3ElementAddress${i}`
            },
                containerPop)


            const popUpButtonModifyElementAddress = html('button', {

                margin: `${((i % 4) == 0 || (i % 4) == 1) ? ((10 * ((i % 4) + 1) + ((4.5 * (1 + (i % 4)))))) : ((10 * ((i % 4) + 1) + ((8.5 * (i % 4)))))}vh 0vh 0vh 8vh`,
            }, {
                textContent: 'MODIFICAR',
                id: `popUpButtonModifyElementAddress${i}`,
                class: 'popUpButtonModifyElementAddressBook',
            },
                containerPop)


            popUpButtonModifyElementAddress.addEventListener('mouseenter', () => {
                popUpButtonModifyElementAddress.style.backgroundColor = '#45b24e';
                popUpButtonModifyElementAddress.style.color = '#285e2d';

                popUpH3ElementAddress.style.color = '#9e9ffe';
                popUpH3ElementAddress.style.textShadow = '2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000';
            })


            popUpButtonModifyElementAddress.addEventListener('mouseleave', () => {
                popUpButtonModifyElementAddress.style.backgroundColor = '#285e2d';
                popUpButtonModifyElementAddress.style.color = '#45b24e';

                popUpH3ElementAddress.style.color = '#414394';
                popUpH3ElementAddress.style.textShadow = '0px 0px 0px transparent';
            })

            popUpButtonModifyElementAddress.addEventListener('click', () => {
                showInputAddressBookModifyUser(
                    popUpInputElementAddress,
                    popUpButtonSaveElementAddress,
                    popUpButtonCancelElementAddress,
                    popUpH3ElementAddress,
                    popUpButtonModifyElementAddress,
                    popUpButtonDeleteElementAddress,
                )
            })




            const popUpButtonDeleteElementAddress = html('button', {
                margin: `${((i % 4) == 0 || (i % 4) == 1) ? ((10 * ((i % 4) + 1) + ((4.5 * (1 + (i % 4)))))) : ((10 * ((i % 4) + 1) + ((8.5 * (i % 4)))))}vh 0vh 0vh 30vh`,
            }, {
                textContent: 'ELIMINAR',
                id: `popUpButtonDeleteElementAddress${i}`,
                class: 'popUpButtonDeleteElementAddressBook',
            },
                containerPop)



            popUpButtonDeleteElementAddress.addEventListener('mouseenter', () => {
                popUpButtonDeleteElementAddress.style.backgroundColor = '#c43b3b';
                popUpButtonDeleteElementAddress.style.color = '#742929';


                popUpH3ElementAddress.style.color = '#9e9ffe';
                popUpH3ElementAddress.style.textShadow = '2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000';
            })


            popUpButtonDeleteElementAddress.addEventListener('mouseleave', () => {
                popUpButtonDeleteElementAddress.style.backgroundColor = '#742929';
                popUpButtonDeleteElementAddress.style.color = '#c43b3b';

                popUpH3ElementAddress.style.color = '#414394';
                popUpH3ElementAddress.style.textShadow = '0px 0px 0px transparent';
            })


            popUpButtonDeleteElementAddress.addEventListener('click', () => {
                showPopUpConfirmationDeleteAddress(popUp2, userData.address[i], i);
            })

            const popUpInputElementAddress = html('input', {
                margin: `${(4 * ((i % 4) + 1) + (13 * (i % 4)))}vh 0vh 0vh 6vh`,
                display: 'none',
            }, {
                class: 'popUpInputElementAddressBook',
                id: `popUpInputElementAddress${i}`,
                placeholder: userData.address[i]
            },
                containerPop)



            const popUpButtonSaveElementAddress = html('button', {

                margin: `${((i % 4) == 0 || (i % 4) == 1) ? ((10 * ((i % 4) + 1) + ((4.5 * (1 + (i % 4)))))) : ((10 * ((i % 4) + 1) + ((8.5 * (i % 4)))))}vh 0vh 0vh 8vh`,
                display: 'none',

            }, {
                textContent: 'GUARDAR',
                id: `popUpButtonSaveElementAddress${i}`,
                class: 'popUpButtonModifyElementAddressBook',
            },
                containerPop)


            popUpButtonSaveElementAddress.addEventListener('mouseenter', () => {
                popUpButtonSaveElementAddress.style.backgroundColor = '#45b24e';
                popUpButtonSaveElementAddress.style.color = '#285e2d';

                popUpH3ElementAddress.style.color = '#9e9ffe';
                popUpH3ElementAddress.style.textShadow = '2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000';
            })


            popUpButtonSaveElementAddress.addEventListener('mouseleave', () => {
                popUpButtonSaveElementAddress.style.backgroundColor = '#285e2d';
                popUpButtonSaveElementAddress.style.color = '#45b24e';

                popUpH3ElementAddress.style.color = '#414394';
                popUpH3ElementAddress.style.textShadow = '0px 0px 0px transparent';
            })


            popUpButtonSaveElementAddress.addEventListener('click', () => {

                const addressCleaned = validateAddressAfterSavedMyAccount(popUpInputElementAddress.value)

                const htmlElementsId = {
                    popUpInputElementAddress: popUpInputElementAddress,
                    popUpButtonSaveElementAddress: popUpButtonSaveElementAddress,
                    popUpButtonCancelElementAddress: popUpButtonCancelElementAddress,
                    popUpH3ElementAddress: popUpH3ElementAddress,
                    popUpButtonModifyElementAddress: popUpButtonModifyElementAddress,
                    popUpButtonDeleteElementAddress: popUpButtonDeleteElementAddress,
                };
                if (addressCleaned.boolean == true) {
                    updateAddress(addressCleaned.address, i, htmlElementsId, popUp2)
                } else {
                    showPopUpMsj(addressCleaned.msj.toUpperCase(), '10vh 0vh 0vh 30vh', popUp2)
                    showH3AddressBookModifyUser(
                        htmlElementsId.popUpInputElementAddress,
                        htmlElementsId.popUpButtonSaveElementAddress,
                        htmlElementsId.popUpButtonCancelElementAddress,
                        htmlElementsId.popUpH3ElementAddress,
                        htmlElementsId.popUpButtonModifyElementAddress,
                        htmlElementsId.popUpButtonDeleteElementAddress,
                    )
                }
                popUpInputElementAddress.value = '';
            })




            const popUpButtonCancelElementAddress = html('button', {
                margin: `${((i % 4) == 0 || (i % 4) == 1) ? ((10 * ((i % 4) + 1) + ((4.5 * (1 + (i % 4)))))) : ((10 * ((i % 4) + 1) + ((8.5 * (i % 4)))))}vh 0vh 0vh 30vh`,
                display: 'none',
            }, {
                textContent: 'CANCELAR',
                id: `popUpButtonDeleteElementAddress${i}`,
                class: 'popUpButtonDeleteElementAddressBook',
            },
                containerPop)



            popUpButtonCancelElementAddress.addEventListener('mouseenter', () => {
                popUpButtonCancelElementAddress.style.backgroundColor = '#c43b3b';
                popUpButtonCancelElementAddress.style.color = '#742929';


                popUpH3ElementAddress.style.color = '#9e9ffe';
                popUpH3ElementAddress.style.textShadow = '2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000';
            })


            popUpButtonCancelElementAddress.addEventListener('mouseleave', () => {
                popUpButtonCancelElementAddress.style.backgroundColor = '#742929';
                popUpButtonCancelElementAddress.style.color = '#c43b3b';

                popUpH3ElementAddress.style.color = '#414394';
                popUpH3ElementAddress.style.textShadow = '0px 0px 0px transparent';
            })

            popUpButtonCancelElementAddress.addEventListener('click', () => {
                popUpInputElementAddress.value = '';
                showH3AddressBookModifyUser(
                    popUpInputElementAddress,
                    popUpButtonSaveElementAddress,
                    popUpButtonCancelElementAddress,
                    popUpH3ElementAddress,
                    popUpButtonModifyElementAddress,
                    popUpButtonDeleteElementAddress,
                )
            })

        }
    }


}

const showPopUpAddressBook = () => {


    const popUp2 = html('div', {
        backgroundImage: 'url("../../imgs/adressBook.png")',
        backgroundSize: 'cover',
        display: 'inline',
        position: 'absolute',
        width: '125vh',
        height: '75vh',
        margin: `-95vh 0vh 0vh 35vh`,
        zIndex: 1000,


    }, {
        id: 'popUp'
    })



    showAddressMyAccount(popUp2)


    const actionNextPageAddress = html('button', {
        margin: "55vh 0vh 0vh 100vh",
        backgroundImage: 'url("../../imgs/address-arrow-next.png")',
    }, {
        id: 'actionNextPageAddress',
        class: 'popUpButtonChangePageElementAddressBook',
    },
        popUp2)

    actionNextPageAddress.addEventListener('mouseenter', () => {
        actionNextPageAddress.style.backgroundImage = 'url("../../imgs/address-arrow-next-active.png")';
        actionNextPageAddress.style.backgroundColor = '#8e90d6';
    })


    actionNextPageAddress.addEventListener('mouseleave', () => {
        actionNextPageAddress.style.backgroundImage = 'url("../../imgs/address-arrow-next.png")';
        actionNextPageAddress.style.backgroundColor = '#5178a1';
    })

    actionNextPageAddress.addEventListener('click', () => {
        const auxCurrentPage = currentMyAccount + 1;

        if ((userData.address.length / 4) > auxCurrentPage) {
            currentMyAccount += 1;
            pageCounter.textContent = currentMyAccount + 1;
            showAddressMyAccount(popUp2)
        }

    })


    const actionPreviousPageAddress = html('button', {
        margin: "55vh 0vh 0vh 70vh",
        backgroundImage: 'url("../../imgs/address-arrow-prev.png")',
    }, {
        id: 'actionPreviousPageAddress',
        class: 'popUpButtonChangePageElementAddressBook',
    },
        popUp2)


    actionPreviousPageAddress.addEventListener('mouseenter', () => {
        actionPreviousPageAddress.style.backgroundImage = 'url("../../imgs/address-arrow-prev-active.png")';
        actionPreviousPageAddress.style.backgroundColor = '#8e90d6';

    })


    actionPreviousPageAddress.addEventListener('mouseleave', () => {
        actionPreviousPageAddress.style.backgroundImage = 'url("../../imgs/address-arrow-prev.png")';
        actionPreviousPageAddress.style.backgroundColor = '#5178a1';
    })

    actionPreviousPageAddress.addEventListener('click', () => {
        const auxCurrentPage = currentMyAccount - 1;
        if (auxCurrentPage >= 0) {
            currentMyAccount -= 1
            pageCounter.textContent = currentMyAccount + 1;
            showAddressMyAccount(popUp2)
        }
    })


    const actionSaveChangeAddress = html('button', {
        margin: "8vh 0vh 0vh 76vh",
        backgroundColor: '#285e2d',
        color: '#45b24e',
    }, {
        textContent: 'GUARDAR CAMBIOS',
        id: 'actionSaveChangeAddress',
        class: 'popUpButtonActionsElementAddressBook',
    },
        popUp2)


    actionSaveChangeAddress.addEventListener('mouseenter', () => {
        actionSaveChangeAddress.style.backgroundColor = '#45b24e';
        actionSaveChangeAddress.style.color = '#285e2d';
    })


    actionSaveChangeAddress.addEventListener('mouseleave', () => {
        actionSaveChangeAddress.style.backgroundColor = '#285e2d';
        actionSaveChangeAddress.style.color = '#45b24e';
    })


    const actionAddAddress = html('button', {
        margin: "23vh 0vh 0vh 76vh",
        backgroundColor: '#285e2d',
        color: '#45b24e',
    }, {
        textContent: 'AGREGAR DOMICILIO',
        id: 'actionAddAddress',
        class: 'popUpButtonActionsElementAddressBook',
    },
        popUp2)


    actionAddAddress.addEventListener('mouseenter', () => {
        actionAddAddress.style.backgroundColor = '#45b24e';
        actionAddAddress.style.color = '#285e2d';
    })


    actionAddAddress.addEventListener('mouseleave', () => {
        actionAddAddress.style.backgroundColor = '#285e2d';
        actionAddAddress.style.color = '#45b24e';
    })

    actionAddAddress.addEventListener('click', () => {
        createNewAddressPopUp(popUp2)
    })



    const actionCancelChangeAddress = html('button', {
        margin: "38vh 0vh 0vh 76vh",
        backgroundColor: '#742929',
        color: '#c43b3b',
    }, {
        textContent: 'CANCELAR CAMBIOS',
        id: 'actionCancelChangeAddress',
        class: 'popUpButtonActionsElementAddressBook',
    },
        popUp2)


    actionCancelChangeAddress.addEventListener('mouseenter', () => {
        actionCancelChangeAddress.style.backgroundColor = '#c43b3b';
        actionCancelChangeAddress.style.color = '#742929';
    })


    actionCancelChangeAddress.addEventListener('mouseleave', () => {
        actionCancelChangeAddress.style.backgroundColor = '#742929';
        actionCancelChangeAddress.style.color = '#c43b3b';
    })

    actionCancelChangeAddress.addEventListener('click', () => {
        const UIfirstLayer = document.getElementById('UIfirstLayer');
        preventUserLoseInfo(UIfirstLayer)
    })


    const pageCounter = html('h3', {
        margin: "61vh 0vh 0vh 89vh",
    }, {
        id: 'pageCounter',
        textContent: currentMyAccount + 1,
        class: 'popUpButtonCurrentPageElementAddressBook',
    },
        popUp2)




}
